package com.livehealthier.mobile.google_analytics;

import java.util.HashMap;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;

public class LH_Google_Analytics extends LH_AnalyticsHandler {
	public enum TrackerName {
		APP_TRACKER, GLOBAL_TRACKER,
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public Tracker getTracker(TrackerName trackerId, Context ctx) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(ctx);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);

			Tracker lh_tracker = analytics
					.newTracker(LHApplication.CurrentEnvironment
							.getAnalyticsPropertyID());

			mTrackers.put(trackerId, lh_tracker);

		}
		return mTrackers.get(trackerId);
	}

	@Override
	public synchronized void screenAnalyticsHits(Context applicationContext,
			String screenName) {

		try {
			
			Log.v("Tracking screen", screenName);
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);

			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);

			lh_tracker.send(new HitBuilders.AppViewBuilder().build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public synchronized void screenAnalyticsHits(Context applicationContext,
			String screenName, String clientName) {

		try {
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);
			lh_tracker.set("client Name", clientName);

			lh_tracker.send(new HitBuilders.AppViewBuilder().setCustomDimension(1, clientName).build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public synchronized void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId) {

		try {
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker.enableAdvertisingIdCollection(true);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);

			lh_tracker.send(new HitBuilders.EventBuilder()
					.setCategory(categoryId).setAction(actionId)
					.setLabel(labelId).build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private Tracker setCommonProperty(Tracker lh_tracker,
			Context applicationContext, String screenName) {
		// TODO Auto-generated method stub
		try {
			lh_tracker.setAppName(applicationContext
					.getString(R.string.app_name));
			
			
		
			String	AppVersion = applicationContext
						.getApplicationContext()
						.getPackageManager()
						.getPackageInfo(
								applicationContext.getApplicationContext()
										.getPackageName(), 0).versionName;

			
			
			lh_tracker.setAppVersion(AppVersion);
			
			lh_tracker.enableAdvertisingIdCollection(true);
			screenName = screenName + "| Android";
			lh_tracker.setScreenName(screenName);
			lh_tracker.set("model", android.os.Build.MODEL);
			lh_tracker.set("device", android.os.Build.DEVICE);
			lh_tracker.set("device_display_name", android.os.Build.DISPLAY);
			lh_tracker.set("brand", android.os.Build.BRAND);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lh_tracker;
	}

	@Override
	public synchronized void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId, String clientName) {

		try {
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);
			lh_tracker.set("client Name", clientName);

			lh_tracker.send(new HitBuilders.EventBuilder()
					.setCategory(categoryId).setAction(actionId)
					.setLabel(labelId).setCustomDimension(1, clientName).build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	
	@Override
	public synchronized void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId, String campaign_name,int index) {

		try {
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);
		//	lh_tracker.set("client Name", clientName);
			lh_tracker.set("campaign_name", campaign_name);

			lh_tracker.send(new HitBuilders.EventBuilder()
					.setCategory(categoryId).setAction(actionId)
					.setLabel(labelId).setCustomDimension(index, campaign_name).build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void exceptionAnalyticsHits(Context applicationContext,
			String screenName, Throwable e) {
		try {
			GoogleAnalytics analytics = GoogleAnalytics
					.getInstance(applicationContext);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);

			Tracker lh_tracker = getTracker(TrackerName.APP_TRACKER,
					applicationContext);

			lh_tracker = setCommonProperty(lh_tracker, applicationContext,
					screenName);

			lh_tracker.send(new HitBuilders.AppViewBuilder().build());

			lh_tracker.send(new HitBuilders.ExceptionBuilder()
					.setDescription(
							new StandardExceptionParser(applicationContext,
									null).getDescription(Thread.currentThread()
									.getName(), e)).setFatal(false).build());

			analytics.dispatchLocalHits();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	

}
