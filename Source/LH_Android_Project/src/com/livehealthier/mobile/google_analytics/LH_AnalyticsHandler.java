package com.livehealthier.mobile.google_analytics;

import android.content.Context;

import com.livehealthier.mobile.utils.LH_Constants.Analytics;

public abstract class LH_AnalyticsHandler {

	protected static final String analytics = Analytics.GOOGLEANALYTICS;
	private static LH_AnalyticsHandler analyticshandler = null;

	public abstract void initialize();

	public abstract void screenAnalyticsHits(Context applicationContext,
			String screenName);

	public abstract void exceptionAnalyticsHits(Context applicationContext,
			String screenName, Throwable e);

	public abstract void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId);

	public abstract void screenAnalyticsHits(Context applicationContext,
			String screenName, String clientName);

	public abstract void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId, String clientName);

	@SuppressWarnings("unused")
	public static LH_AnalyticsHandler getInstance() {
		if (null != analyticshandler) {
			return analyticshandler;
		}

		if (analytics == Analytics.GOOGLEANALYTICS) {
			analyticshandler = new LH_Google_Analytics();
		} else if (analytics == Analytics.NULLANALYTICS) {
			analyticshandler = new LH_Null_Analytics();
		}

		// analyticshandler.initialize();

		return analyticshandler;
	}



	public abstract void eventAnalyticsHits(Context applicationContext,
			String screenName, String categoryId, String actionId,
			String labelId, String campaign_name, int index) ;

}
