package com.livehealthier.mobile.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;

public class LH_User_Database extends SQLiteOpenHelper {

	private static LH_User_Database mInstance;

	/**
	 * the name of the table
	 */
	public static final String LOCALSTORAGE_TABLE_NAME = "lh_user_information";

	private static final int DATABASE_VERSION = 7;
	private static final String DATABASE_NAME = "lh_storage.db";
	private static final String DICTIONARY_TABLE_CREATE = "CREATE TABLE "
			+ LOCALSTORAGE_TABLE_NAME + " (" + LH_Constants.LH_ID
			+ " TEXT NULL, " + LH_Constants._FIRSTNAME + " TEXT NULL, "
			+ LH_Constants._LASTNAME + " TEXT NULL, " + LH_Constants._EMAIL
			+ " TEXT NULL, " + LH_Constants._EXTERNALID + " TEXT NULL, "
			+ LH_Constants._TOKEN + " TEXT NULL, " + LH_Constants._SESSIONID
			+ " TEXT NULL, " + LH_Constants._CLIENTACCOUNTID + " TEXT NULL, "
			+ LH_Constants._CLIENTACCOUNTNAME + " TEXT NULL, "
			+ LH_Constants._SECONDARYLOGOPATH + " TEXT NULL, "
			+ LH_Constants._WELLNESSBANKPROGRAM + " TEXT NULL, "
			+ LH_Constants._OFFICEID + " TEXT NULL, "
			+ LH_Constants._OFFICECODE + " TEXT NULL, "
			+ LH_Constants._OFFICENAME + " TEXT NULL, "
			+ LH_Constants._MOBILELOGOURL + " TEXT NULL, "
			+ LH_Constants._PRIMARYCOLOR + " TEXT NULL, "
			+ LH_Constants._SECONDARYCOLOR + " TEXT NULL);";

	/**
	 * Returns an instance of LocalStorage
	 * 
	 * @param ctx
	 *            : a Context used to create the database
	 * @return the instance of LocalStorage of the application or a new one if
	 *         it has not been created before.
	 */
	public static LH_User_Database getInstance(Context ctx) {
		if (mInstance == null) {
			mInstance = new LH_User_Database(ctx.getApplicationContext());
		}
		return mInstance;
	}

	private LH_User_Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DICTIONARY_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		LH_LogHelper.logWarning("Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + LOCALSTORAGE_TABLE_NAME);
		onCreate(db);
	}
}
