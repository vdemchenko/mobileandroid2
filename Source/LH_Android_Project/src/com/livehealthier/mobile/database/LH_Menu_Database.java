package com.livehealthier.mobile.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;

public class LH_Menu_Database extends SQLiteOpenHelper {

	private static LH_Menu_Database mInstance;

	/**
	 * the name of the table
	 */
	public static final String LOCALSTORAGE_TABLE_NAME = "lh_menu_information";

	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_NAME = "lh_menu_storage.db";
	private static final String DICTIONARY_TABLE_CREATE = "CREATE TABLE "
			+ LOCALSTORAGE_TABLE_NAME + " (" + LH_Constants.LH_ID
			+ " TEXT PRIMARY KEY, " + LH_Constants._HASSUBMENU + " TEXT NULL, "
			+ LH_Constants._SUBMENU + " TEXT NULL, " + LH_Constants._TITLE
			+ " TEXT NULL, " + LH_Constants._TYPE + " TEXT NULL, "
			+ LH_Constants._UNIQUEKEY + " TEXT NULL, "
			+ LH_Constants._SUB_HASSUBMENU + " TEXT NULL, "
			+ LH_Constants._SUB_SUBMENU + " TEXT NULL, "
			+ LH_Constants._SUB_TITLE + " TEXT NULL, " + LH_Constants._SUB_TYPE
			+ " TEXT NULL, " + LH_Constants._SUB_UNIQUEKEY + " TEXT NULL, "
			+ LH_Constants._SUB_URL + " TEXT NULL, " + LH_Constants._URL
			+ " TEXT NULL);";

	/**
	 * Returns an instance of LocalStorage
	 * 
	 * @param ctx
	 *            : a Context used to create the database
	 * @return the instance of LocalStorage of the application or a new one if
	 *         it has not been created before.
	 */
	public static LH_Menu_Database getInstance(Context ctx) {
		if (mInstance == null) {
			mInstance = new LH_Menu_Database(ctx.getApplicationContext());
		}
		return mInstance;
	}

	private LH_Menu_Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DICTIONARY_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		LH_LogHelper.logWarning("Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");

		db.execSQL("DROP TABLE IF EXISTS " + LOCALSTORAGE_TABLE_NAME);
		onCreate(db);
	}
}
