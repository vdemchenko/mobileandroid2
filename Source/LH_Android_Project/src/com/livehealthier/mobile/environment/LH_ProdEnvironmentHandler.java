package com.livehealthier.mobile.environment;

public class LH_ProdEnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "https://lhapi.livehealthier.com/";
		super.gadgetDeeplinkBaseUrl="http://gadgetv2.qa-web01.livehealthier.com/default.aspx";
		super.analyticsPropertyID = "UA-42895889-6";
		super.isVerboseLoggingEnabled = false;
		super.isDebugLoggingEnabled = false;
	}

}
