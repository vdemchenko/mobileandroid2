package com.livehealthier.mobile.environment;

import com.livehealthier.mobile.utils.LH_Constants.Environments;

public abstract class LH_EnvironmentHandler {


	protected static final String environment = Environments.QA;

	protected String apiBaseUrl;
	protected String gadgetDeeplinkBaseUrl;
	protected String analyticsPropertyID;
	protected boolean isVerboseLoggingEnabled;
	protected boolean isDebugLoggingEnabled;
	private static LH_EnvironmentHandler environmentHandler = null;

	public abstract void initialize();

	public String getApiBaseUrl() {
		return apiBaseUrl;
	}

	public String getAnalyticsPropertyID() {
		return analyticsPropertyID;
	}

	public String getgadgetDeeplinkBaseUrl() {
		return gadgetDeeplinkBaseUrl;
	}
	public boolean getIsVerboseLoggingEnabled() {
		return isVerboseLoggingEnabled;
	}

	public boolean getIsDebugLoggingEnabled() {
		return isDebugLoggingEnabled;
	}

	@SuppressWarnings("unused")
	public static LH_EnvironmentHandler getInstance() {
		if (null != environmentHandler) {
			return environmentHandler;
		}

		if (environment == Environments.QA) {
			environmentHandler = new LH_QAEnvironmentHandler();
		} else if (environment == Environments.QA2) {
			environmentHandler = new LH_QA2EnvironmentHandler();
		} else if (environment == Environments.STAGE) {
			environmentHandler = new LH_StagingEnvironmentHandler();
		} else if (environment == Environments.PROD) {
			environmentHandler = new LH_ProdEnvironmentHandler();
		} else if (environment == Environments.ALLSTATE) {
			environmentHandler = new LH_AllStateEnviornmentHandler();
		} else if (environment == Environments.ALLSTATEQA) {
			environmentHandler = new LH_AllStateQAEnvironmentHandler();
		}
		environmentHandler.initialize();

		return environmentHandler;
	}
}
