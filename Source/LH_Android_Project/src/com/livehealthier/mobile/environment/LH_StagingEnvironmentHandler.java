package com.livehealthier.mobile.environment;

public class LH_StagingEnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "http://lhmobileapi.staging.livehealthier.com/";
		super.gadgetDeeplinkBaseUrl="http://gadgetv2.qa-web01.livehealthier.com/default.aspx";
		super.analyticsPropertyID = "UA-58583957-2";
		super.isVerboseLoggingEnabled = true;
		super.isDebugLoggingEnabled = true;
	}
}
