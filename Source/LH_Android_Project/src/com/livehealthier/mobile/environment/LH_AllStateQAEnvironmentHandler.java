package com.livehealthier.mobile.environment;

public class LH_AllStateQAEnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "http://api.v2.qa-web01.livehealthier.com/";
		super.analyticsPropertyID = "UA-59090009-2";
		super.isVerboseLoggingEnabled = false;
		super.isDebugLoggingEnabled = false;
	}

}
