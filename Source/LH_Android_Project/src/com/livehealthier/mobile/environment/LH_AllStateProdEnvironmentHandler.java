package com.livehealthier.mobile.environment;

public class LH_AllStateProdEnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "https://lhapiallstate.livehealthier.com/";
		super.gadgetDeeplinkBaseUrl="http://gadgetv2.qa-web01.livehealthier.com/default.aspx";
		super.analyticsPropertyID = "UA-42895889-9";
		super.isVerboseLoggingEnabled = false;
		super.isDebugLoggingEnabled = false;
	}

}
