package com.livehealthier.mobile.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.api.LHComponentRegistryAPI;
import com.livehealthier.mobile.api.LH_AppVersionAPI;
import com.livehealthier.mobile.api.LH_AuthHandler;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistry;
import com.livehealthier.mobile.environment.LH_EnvironmentHandler;
import com.livehealthier.mobile.google_analytics.LH_AnalyticsHandler;
import com.livehealthier.mobile.model.LH_Menu;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_ExceptionHandler;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LHApplication extends Application {

	private static final String SHARED_PREFS_FILE = "CurrentDateInformation";
	public static LHApplication LHApplicationContext;
	public static Context LHAscnTaskContext;
	public static LH_Menu Menu;
	public static String Url;
	public static boolean emptystack = false;;
	public static int WebViewTimeoutCounter;
	public static Boolean RefreshAttemptErrored = false;
	public static Boolean ForcedTokenRequest = false;
	public static Date TokenUpdatedAt;
	// public static String UserName;
	// public static String UserPwd;

	public static int LoginPageRedirectCounter = 0;
	public static LH_ComponentRegistry ComponentRegistry;

	public static LH_EnvironmentHandler CurrentEnvironment;
	public static LH_AnalyticsHandler analyticsHandler;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@SuppressWarnings("static-access")
	@Override
	public void onCreate() {

		LHApplicationContext = this;
		LHAscnTaskContext = getApplicationContext();
	
		ComponentRegistry = new LH_ComponentRegistry();
		// ComponentRegistry.initializeRegistry();
		//
		CurrentEnvironment = LH_EnvironmentHandler.getInstance();
		analyticsHandler = LH_AnalyticsHandler.getInstance();
		try {
			Thread.currentThread().setDefaultUncaughtExceptionHandler(
					new LH_ExceptionHandler(getApplicationContext()));

		} catch (Exception e) {
			e.printStackTrace();

		}

		super.onCreate();

	}

	@Override
	public void onLowMemory() {

		super.onLowMemory();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	@SuppressWarnings("deprecation")
	public static void ClearCookies() {
		CookieSyncManager cookieSyncManager = CookieSyncManager
				.createInstance(LHApplicationContext);
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeSessionCookie();
		cookieManager.removeAllCookie();
		cookieSyncManager.sync();
	}

	/*
	 * This method should be called to set the current url that is being loaded
	 * on the web view as a result of the menu click
	 */
	public static void SetMenuClick(String webUrl) {

		if (webUrl.length() > 1) {
			LHApplication.LoginPageRedirectCounter = 0;
			LHApplication.ComponentRegistry.setLastUserInteractionItem(webUrl);
		}
		// LHApplication.Url = webUrl;
		// LHApplication.WebViewTimeoutCounter = 0;
		// LHApplication.RefreshAttemptErrored = false;
		// LHApplication.ForcedTokenRequest = false;

	}

	/*
	 * This method will return minutes elapsed from the time that the token was
	 * last generated
	 */
	public static long MinutesFromLastUpdateToToken() {
		if (LHApplication.TokenUpdatedAt.equals(null)) {
			LHApplication.TokenUpdatedAt = new Date();
			return 0;
		}
		Date currentTime = new Date();
		long diff = currentTime.getTime()
				- LHApplication.TokenUpdatedAt.getTime();
		return diff / (60 * 1000);
	}

	public static void ResetErrorFlagsForWebView() {
		if (LHApplication.MinutesFromLastUpdateToToken() > 10) {
			LHApplication.WebViewTimeoutCounter = 0;
			LHApplication.RefreshAttemptErrored = false;
			LHApplication.ForcedTokenRequest = false;
		}
	}

	public static void LogoutUser(Activity context) {
		LH_Utils.savelogintoken(context, "livehealthier");

		LH_AuthHandler authHandler = new LH_AuthHandler(context);
		authHandler.execute("");

		Intent intent = new Intent(context,
				com.livehealthier.mobile.fragment.LH_Login_Home.class);

		context.startActivity(intent);

	}

	public static void getComponentRegistryUpdate(String Login) {

		String AppVersion = "";

		try {
			AppVersion = LHApplicationContext
					.getApplicationContext()
					.getPackageManager()
					.getPackageInfo(
							LHApplicationContext.getApplicationContext()
									.getPackageName(), 0).versionName;
	
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (null == AppVersion) {
			AppVersion = LHApplicationContext.getApplicationContext()
					.getString(R.string.Versionname);

			// cookieManager.setCookie(urlRequested, "AppVersion=" +
			// AppVersion);
		}

		if (Login.equalsIgnoreCase("Auto")) {
			if (checkDateDifferent()) {
				UpdateDate();
				String componentAPIURL = LHApplication.CurrentEnvironment
						.getApiBaseUrl()
						+ LH_ApiUrls.COMPONENTREGISTRYAPI
						+ AppVersion;

		
				LH_LogHelper.logInfo(componentAPIURL);
				new LHComponentRegistryAPI(componentAPIURL)
						.execute("ComponentAPI");
				
			
				LHApplication.ComponentRegistry.initializeRegistry();
			}
		} else {

			UpdateDate();

			String componentAPIURL = LHApplication.CurrentEnvironment
					.getApiBaseUrl()
					+ LH_ApiUrls.COMPONENTREGISTRYAPI
					+ AppVersion;
			LH_LogHelper.logInfo(componentAPIURL);

			new LHComponentRegistryAPI(componentAPIURL).execute("ComponentAPI");
			
			
		}
		
	}

	private static boolean UpdateDate() {
		Date currentTime = new Date();
		SharedPreferences prefs = LHApplicationContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		//Date PreviousTime = null;
		//String PreviousDate = prefs.getString("Date", "");
		editor.putString("Date", currentTime.toString());
		 editor.commit();
		 return true;

	}

	static boolean UpdateAppversionDate() {
		Date currentTime = new Date();
		SharedPreferences prefs = LHApplicationContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		//Date PreviousTime = null;
		//String PreviousDate = prefs.getString("Date", "");
		editor.putString("AppversionDate", currentTime.toString());
		 editor.commit();
		 return true;

	}

		
	

	public static boolean checkDateDifferent() {
		Date currentTime = new Date();
		SharedPreferences prefs = LHApplicationContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		//Editor editor = prefs.edit();
		Date PreviousTime = null;
		String PreviousDate = prefs.getString("Date", "");
//		editor.putString("Date", currentTime.toString());
//		editor.commit();
		if (PreviousDate == "") {

			return true;

		}

		SimpleDateFormat format = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss zzz yyyy");

		try {
			PreviousTime = format.parse(PreviousDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long diff = currentTime.getTime() - PreviousTime.getTime();

		long diffHours = diff / (60 * 60 * 1000) % 24;

		if (diffHours > 23) {
			return true;
		} else {
			return false;
		}

	}
	
	public static boolean checkAppVersionDateDifferent() {
		Date currentTime = new Date();
		SharedPreferences prefs = LHApplicationContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		//Editor editor = prefs.edit();
		Date PreviousTime = null;
		String PreviousDate = prefs.getString("AppversionDate", "");
//		editor.putString("Date", currentTime.toString());
//		editor.commit();
		if (PreviousDate == "") {

			return true;

		}

		SimpleDateFormat format = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss zzz yyyy");

		try {
			PreviousTime = format.parse(PreviousDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long diff = currentTime.getTime() - PreviousTime.getTime();

		long diffHours = diff / (60 * 60 * 1000) % 24;

		if (diffHours > 23) {
			return true;
		} else {
			return false;
		}

	}


}
