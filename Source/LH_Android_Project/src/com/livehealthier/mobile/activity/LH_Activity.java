package com.livehealthier.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.fragment.LH_Home;
import com.livehealthier.mobile.fragment.LH_Menu_List_Fragment;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;

public class LH_Activity extends LHActivityBase {

	private Fragment mContent;
	public static LH_Activity lhActivity;

	public LH_Activity() {
		super(R.string.app_name);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// setSlidingActionBarEnabled(true);
		if (savedInstanceState != null) {
			mContent = getSupportFragmentManager().getFragment(
					savedInstanceState, "mContent");
		}
		if (mContent == null) {
			mContent = new LH_Home();
		}

		String Title="";
		String Url="";
		try {
			Intent intent = getIntent();
			Title = intent.getStringExtra(LH_Constants._TITLE);
			Url = intent.getStringExtra(LH_Constants._URL);
			if(null!=Title && Title.length()>2){
			Bundle bundle = new Bundle();
			bundle.putString(LH_Constants._TITLE, Title);
			bundle.putString(LH_Constants._URL, Url);
			mContent.setArguments(bundle);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// set the Above View
		setContentView(R.layout.content_frame);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, mContent).commit();
		lhActivity = this;
		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		// getSupportFragmentManager().beginTransaction()
		// .replace(R.id.menu_frame, new LH_Menu_List_Fragment()).commit();

	}

	public void switchContent(Fragment fragment) {
		mContent = fragment;
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, fragment).commit();
		getSlidingMenu().showContent();
	}

	@Override
	public void onBackPressed() {

		LH_LogHelper.logInfo("Back key pressed");

		String currentTitle = LHApplication.ComponentRegistry
				.getLastUserInteractionItemTitle();

		if ((null == LHApplication.Menu)
				|| (null != LHApplication.Menu && currentTitle
						.equalsIgnoreCase(LHApplication.Menu
								.getLhmenuitemList().get(0).getTitle()))) {
			
			
			this.finish();
			super.onBackPressed();
		} else {
			
		
			getSupportFragmentManager().popBackStack();
			String prevTitle = LHApplication.ComponentRegistry
					.getPreviousAccessedComponentTitle();
		
			LH_Menu_List_Fragment.LhMenuFragment.setMenuSelection(prevTitle);
			LHApplication.ComponentRegistry
					.setLastUserInteractionItemOnBackClick(prevTitle);
		
			
		}

	}

	@Override
	public void onResume() {
		super.onResume();

	}

}
