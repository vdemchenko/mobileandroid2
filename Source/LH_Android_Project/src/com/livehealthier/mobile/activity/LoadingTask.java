package com.livehealthier.mobile.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.livehealthier.mobile.R;

import com.livehealthier.mobile.api.LH_MenuHandler;
import com.livehealthier.mobile.api.LH_UserHandler;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_AppVersion;
import com.livehealthier.mobile.model.LH_AppVersionModel;
import com.livehealthier.mobile.model.LH_Login;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LoadingTask extends AsyncTask<String, Integer, Integer> {

	public interface LoadingTaskFinishedListener {
		void onTaskFinished(); // If you want to pass something back to the
								// listener add a param to this method
	}

	LH_AppVersion AppVersionModel;
	Boolean Displaymsg = false;
	String versionName;
	String IsUpgradeOptional;
	String MobileAppVersionId;
	String NumberOfDaysGapBetweenMessageDisplay;
	String UpgradeMessage;
	String VersionText;
	String loginText;
	private static final String SHARED_PREFS_APPVERSION = "APPVersionDateInformation";
	String AppVersion = "";
	private Activity m_context;
	// This is the progress bar you want to update while the task is in progress
	private final ProgressBar progressBar;
	// This is the listener that will be told when this task is finished
	private final LoadingTaskFinishedListener finishedListener;

	private final boolean menu;

	/**
	 * A Loading task that will load some resources that are necessary for the
	 * app to start
	 * 
	 * @param progressBar
	 *            - the progress bar you want to update while the task is in
	 *            progress
	 * @param finishedListener
	 *            - the listener that will be told when this task is finished
	 */
	public LoadingTask(ProgressBar progressBar,
			LoadingTaskFinishedListener finishedListener, Activity m_context,
			boolean menu) {
		this.progressBar = progressBar;
		this.finishedListener = finishedListener;
		this.m_context = m_context;
		this.menu = menu;
	}

	@Override
	protected Integer doInBackground(String... params) {

		if (menu) {
			ShowProgrssbarPart(0, 4, 10);
			// Get user Information here
			// new
			// LH_AppVersionAPI(LHApplication.LHAscnTaskContext).execute("Auto");

			callAppVersionApi("Auto");

			LH_UserHandler userHandler = new LH_UserHandler();
			LH_Login user = userHandler.getUser(m_context);

			if (null != user && user.getStatus().equalsIgnoreCase("success")) {
				// Get menu here
				ShowProgrssbarPart(5, 9, 10);
				LH_MenuHandler menuHandler = new LH_MenuHandler();
				menuHandler.getMenu(m_context);
				if (null == LHApplication.Menu
						|| (null != LHApplication.Menu && LHApplication.Menu
								.getLhmenuitemList().size() == 0)) {
					LH_Utils.savelogintoken(m_context, "livehealthier");
				}

				ShowProgrssbarPart(9, 10, 10);
			} else {
				ShowProgrssbarPart(5, 10, 10);
			}
			LHApplication.getComponentRegistryUpdate("Auto");

			// downloadMenu();
		} else {
			ShowProgrssbarPart(0, 4, 10);
			callAppVersionApi("Login");
			ShowProgrssbarPart(5,8, 10);
			
			
			
			ShowProgrssbarPart(8,10, 10);
			// new
			// LH_AppVersionAPI(LHApplication.LHAscnTaskContext).execute("Login");
			// ShowProgrssbar();
		}

		// Perhaps you want to return something to your post execute
		return 1;
	}

	private void callAppVersionApi(String login) {

		String loginText = login;
		if (loginText.equalsIgnoreCase("Login")) {
			getAppVersionInfo(m_context);
		} else {
			if (LHApplication.checkAppVersionDateDifferent()) {

				getAppVersionInfo(m_context);
				LHApplication.UpdateAppversionDate();
			}

		}

	}

	private void getAppVersionInfo(Activity m_context) {

		try {

			// String token = LH_Utils.getWebToken(context);
			Displaymsg = true;

			try {
				AppVersion = m_context
						.getApplicationContext()
						.getPackageManager()
						.getPackageInfo(
								m_context.getApplicationContext()
										.getPackageName(), 0).versionName;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (null == AppVersion) {
				AppVersion = m_context.getApplicationContext().getString(
						R.string.Versionname);

				// cookieManager.setCookie(urlRequested, "AppVersion=" +
				// AppVersion);
			}

			// LH_LogHelper.logInfo("User Token On Logout " + token);

			String response = LHHttpConnection.getResponseFromGetRequest(
					LHApplication.CurrentEnvironment.getApiBaseUrl()
							+ LH_ApiUrls.MOBILEAPPVERSIONID+AppVersion, "");
			LH_LogHelper.logInfo("AppVersion Url - " + LHApplication.CurrentEnvironment.getApiBaseUrl()
					+ LH_ApiUrls.MOBILEAPPVERSIONID+AppVersion);
			LH_LogHelper.logInfo("AppVersion response - " + response);

			AppVersionModel = LH_Utils.appVersionparser(response);

			List<LH_AppVersionModel> LH_AppVersionModel = AppVersionModel
					.getAppVersion();
			versionName = LH_AppVersionModel.get(0).getVersionName();
			IsUpgradeOptional = LH_AppVersionModel.get(0)
					.getIsUpgradeOptional();
			MobileAppVersionId = LH_AppVersionModel.get(0)
					.getMobileAppVersionId();
			NumberOfDaysGapBetweenMessageDisplay = LH_AppVersionModel.get(0)
					.getNumberOfDaysGapBetweenMessageDisplay();
			UpgradeMessage = LH_AppVersionModel.get(0).getUpgradeMessage();
			VersionText = LH_AppVersionModel.get(0).getVersionText();

		} catch (Exception e) {
			e.printStackTrace();
			Displaymsg = false;
		}

	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);

		try {
			Float applicationVersion = 0.0f;
			Float serverVersion = 0.0f;

			if (Displaymsg) {

				try {

					try {
						applicationVersion = Float.parseFloat(AppVersion);
					} catch (Exception e) {
						
						applicationVersion = 0.0f;
						
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (versionName.length() > 0) {
						try {
							serverVersion = Float.parseFloat(versionName);
						} catch (Exception e) {
							
							serverVersion = 0.0f;
							
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			// displayAlertOptional();
			// updateAppVersionDate();
			if (serverVersion > applicationVersion) {

				if (IsUpgradeOptional.equalsIgnoreCase("true")) {

					if (getNumberOfDaysGapBetweenMessageDisplay()) {

						displayAlertOptional();

						updateAppVersionDate();
					}else{
						finishedListener.onTaskFinished(); // Do nothing
					}

				} else {

					displayAlert();
				}

			} else {

				finishedListener.onTaskFinished(); // Do nothing
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			finishedListener.onTaskFinished(); // Do nothing
		}

		// Tell whoever was listening we have
		// finished
	}

	private Boolean getNumberOfDaysGapBetweenMessageDisplay() {
		try {
			Date currentTime = new Date();
			SharedPreferences prefs = m_context.getSharedPreferences(
					SHARED_PREFS_APPVERSION, Context.MODE_PRIVATE);
			// Editor editor = prefs.edit();
			Date PreviousTime = null;
			String PreviousDate = prefs.getString("AppVersionDate", "");
			// editor.putString("Date", currentTime.toString());
			// editor.commit();
			if (PreviousDate == "") {

				return true;

			}

			SimpleDateFormat format = new SimpleDateFormat(
					"EEE MMM dd HH:mm:ss zzz yyyy");

			try {
				PreviousTime = format.parse(PreviousDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			long diff = currentTime.getTime() - PreviousTime.getTime();

			long diffDays = diff / (24 * 60 * 60 * 1000);

			int NumberOfDays = Integer
					.parseInt(NumberOfDaysGapBetweenMessageDisplay);

			if (diffDays > NumberOfDays) {
				return true;
			} else {
				return false;
			}
		} catch (NumberFormatException e) {
		
		
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}

	}

	private Boolean updateAppVersionDate() {
		Date currentTime = new Date();
		SharedPreferences prefs = m_context.getSharedPreferences(
				SHARED_PREFS_APPVERSION, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		// Date PreviousTime = null;
		// String PreviousDate = prefs.getString("Date", "");
		editor.putString("AppVersionDate", currentTime.toString());
		editor.commit();
		return true;

	}

	@SuppressWarnings("deprecation")
	private void displayAlertOptional() {
		final AlertDialog alertDialog = new AlertDialog.Builder(m_context)
				.create();
		alertDialog.setTitle(m_context.getResources().getString(
				R.string.app_name));
		alertDialog.setMessage(UpgradeMessage);
		alertDialog.setCancelable(false);
		alertDialog.setButton(
				m_context.getResources().getString(R.string.Update),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int which) {

						final String appPackageName = m_context
								.getPackageName(); // getPackageName() from
													// Context or Activity
													// object
						try {
							m_context.startActivity(new Intent(
									Intent.ACTION_VIEW, Uri
											.parse("market://details?id="
													+ appPackageName)));
							LHSplashScreen.lhSplashContext.finish();
						
							LHApplication.analyticsHandler
							.eventAnalyticsHits(
									m_context,
									"" + "AppUpdate",
									m_context
											.getResources()
											.getString(
													R.string.GA_Category),
													m_context
											.getResources()
											.getString(
													R.string.GA_Action_Appupdate_item_pressed),
									"" + "AppUpdate");
						
						} catch (final android.content.ActivityNotFoundException anfe) {
							m_context.startActivity(new Intent(
									Intent.ACTION_VIEW,
									Uri.parse("https://play.google.com/store/apps/details?id="
											+ appPackageName)));
							LHSplashScreen.lhSplashContext.finish();
						}
						// finishedListener.onTaskFinished();
					}
				});

		alertDialog.setButton2(m_context.getResources()
				.getString(R.string.Skip),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						alertDialog.cancel();
						finishedListener.onTaskFinished();
						
						LHApplication.analyticsHandler
						.eventAnalyticsHits(
								m_context,
								"" + "Cancel",
								m_context
										.getResources()
										.getString(
												R.string.GA_Category),
												m_context
										.getResources()
										.getString(
												R.string.GA_Action_Appupdate_item_pressed),
								"" + "Cancel");

					}
				});

		alertDialog.show();

	}

	@SuppressWarnings("deprecation")
	private void displayAlert() {
		final AlertDialog alertDialog = new AlertDialog.Builder(m_context)
				.create();
		alertDialog.setTitle(m_context.getResources().getString(
				R.string.app_name));
		alertDialog.setMessage(UpgradeMessage);
		alertDialog.setCancelable(false);
		alertDialog.setButton(
				m_context.getResources().getString(R.string.Update),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						final String appPackageName = m_context
								.getPackageName(); // getPackageName() from
													// Context or Activity
													// object
						try {
							m_context.startActivity(new Intent(
									Intent.ACTION_VIEW, Uri
											.parse("market://details?id="
													+ appPackageName)));
						
							LHSplashScreen.lhSplashContext.finish();
						
							
							LHApplication.analyticsHandler
							.eventAnalyticsHits(
									m_context,
									"" + "AppUpdate",
									m_context
											.getResources()
											.getString(
													R.string.GA_Category),
													m_context
											.getResources()
											.getString(
													R.string.GA_Action_Appupdate_item_pressed),
									"" + "AppUpdate");
							
						} catch (android.content.ActivityNotFoundException anfe) {
							m_context.startActivity(new Intent(
									Intent.ACTION_VIEW,
									Uri.parse("https://play.google.com/store/apps/details?id="
											+ appPackageName)));
							LHSplashScreen.lhSplashContext.finish();
						}
						// /finishedListener.onTaskFinished();
					}
				});

		alertDialog.show();

	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		progressBar.setProgress(values[0]); // This is ran on the UI thread so
											// it is ok to update our progress
											// bar ( a UI view ) here
	}

	private void ShowProgrssbarPart(int start, int end, int total) {
		for (int i = start; i <= end; i++) {

			// Update the progress bar after every step
			int progress = (int) ((i / (float) total) * 100);
			publishProgress(progress);

			// Do some long loading things
			try {
				Thread.sleep(500);
			} catch (InterruptedException ignore) {
			}
		}
	}
}