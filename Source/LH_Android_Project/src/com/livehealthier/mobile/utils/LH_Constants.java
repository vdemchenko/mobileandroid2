package com.livehealthier.mobile.utils;

import java.util.ArrayList;
import java.util.Arrays;

public class LH_Constants {
	public static final String MyPREFERENCES = "MyPrefs";
	public static final String Token = "livehealthier";

	/**
	 * the id column of the table lh_user_information
	 */
	public static final String LH_ID = "id";
	public static final String _VALUE = "value";
	public static final String _FIRSTNAME = "FirstName";
	public static final String _LASTNAME = "LastName";
	public static final String _EMAIL = "Email";
	public static final String _EXTERNALID = "ExternalID";
	public static final String _TOKEN = "Token";
	public static final String _SESSIONID = "SessionId";
	public static final String _CLIENTACCOUNTID = "ClientAccountID";
	public static final String _CLIENTACCOUNTNAME = "ClientAccountName";
	public static final String _SECONDARYLOGOPATH = "SecondaryLogoPath";

	public static final String _OFFICEID = "OfficeID";
	public static final String _OFFICECODE = "OfficeCode";
	public static final String _OFFICENAME = "OfficeName";
	public static final String _PRIMARYCOLOR = "PrimaryColor";
	public static final String _SECONDARYCOLOR = "SecondaryColor";

	public static final String _MOBILELOGOURL = "MobileLogo";
	public static final String _WELLNESSBANKPROGRAM = "WellnessProgramName";

	/**
	 * the id column of the table lh_menu_information
	 */
	public static final String _HASSUBMENU = "HasSubMenu";
	public static final String _SUBMENU = "SubMenu";
	public static final String _TITLE = "Title";
	public static final String _TYPE = "Type";
	public static final String _UNIQUEKEY = "UniqueKey";
	public static final String _URL = "url";

	/**
	 * the id column of the table lh_menu_information
	 */
	public static final String _SUB_HASSUBMENU = "sub_HasSubMenu";
	public static final String _SUB_SUBMENU = "sub_Menu";
	public static final String _SUB_TITLE = "sub_Title";
	public static final String _SUB_TYPE = "sub_Type";
	public static final String _SUB_UNIQUEKEY = "sub_UniqueKey";
	public static final String _SUB_URL = "sub_url";

	public static class Environments {
		// public static final String DEV = "dev";
		public static final String QA = "qa";
		public static final String QA2 = "qa2";
		// public static final String QA3 = "qa3";
		public static final String STAGE = "stage";
		public static final String PROD = "prod";
		public static final String ALLSTATE = "allstate";
		public static final String ALLSTATEQA = "allstateqa";
	}

	public static class Analytics {
		// public static final String DEV = "dev";
		public static final String GOOGLEANALYTICS = "googleanalytics";
		public static final String NULLANALYTICS = "nullanalytics";
	}

	public static class SpecialPages {
		public static final String PORTAL_MOBILE_AUTO_LOGIN_PAGE = "mobileautologin.aspx?dest=";
		public static final String PORTAL_LOGOUT_PAGE = "logout.aspx";
		public static final String PORTAL_ERROR_PAGE = "merrorpage.aspx";
		public static ArrayList<String> PAGES_FOR_TIMEOUT_REFRESH = new ArrayList<String>(
				Arrays.asList("/merrorpage.aspx", "/login.aspx", "/login.html",
						"/login.htm"));
		public static ArrayList<String> PAGES_FOR_BREAKING_OUT_TO_NATIVE_LOGIN = new ArrayList<String>(
				Arrays.asList("/merrorpage.aspx", "/login.aspx", "/login.html",
						"/login.htm"));

		public static ArrayList<String> PAGES_FOR_EMAIL = new ArrayList<String>(
				Arrays.asList("mailto:"));
		public static final String PAGE_FOR_EMAIL = "mailto:";

	}

	public static class FeatureType {
		public static final String WEB_VIEW = "webview";
		public static final String IONIC = "ionic";
		public static final String NATIVE = "native";
	}

	public static final String LOG_TAG = "com.livehealthier.mobile";
}
