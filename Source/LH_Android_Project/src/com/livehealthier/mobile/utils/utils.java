package com.livehealthier.mobile.utils;

import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.res.Configuration;

public class utils {
	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static String getSizeName(Context context) {
		int screenLayout = context.getResources().getConfiguration().screenLayout;
		screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

		switch (screenLayout) {
		case Configuration.SCREENLAYOUT_SIZE_SMALL:
			return "small";
		case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			return "normal";
		case Configuration.SCREENLAYOUT_SIZE_LARGE:
			return "large";
		case 4: // Configuration.SCREENLAYOUT_SIZE_XLARGE is API >= 9
			return "xlarge";
		default:
			return "undefined";
		}
	}
}
