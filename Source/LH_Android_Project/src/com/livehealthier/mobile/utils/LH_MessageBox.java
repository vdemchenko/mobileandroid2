package com.livehealthier.mobile.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class LH_MessageBox {
	private Context _context;

	public LH_MessageBox(Context context) {
		this._context = context;
	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

}
