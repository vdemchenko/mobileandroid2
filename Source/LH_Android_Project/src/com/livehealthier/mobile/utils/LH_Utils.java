package com.livehealthier.mobile.utils;

import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.livehealthier.mobile.database.LH_Menu_Database;
import com.livehealthier.mobile.database.LH_User_Database;
import com.livehealthier.mobile.model.LH_AppVersion;
import com.livehealthier.mobile.model.LH_ComponentRegistryModel;
import com.livehealthier.mobile.model.LH_Login;
import com.livehealthier.mobile.model.LH_Menu;

public class LH_Utils {
	private static JsonFactory jsonFactory = null;
	private static ObjectMapper objectMapper = null;
	private static JsonParser jp = null;
	static SharedPreferences sharedpreferences;

	private static void initObjectmapper() {
		try {
			objectMapper = new ObjectMapper();

			objectMapper
					.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

			objectMapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE,
					false);
			objectMapper.configure(
					DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			objectMapper.configure(
					DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
					true);
			objectMapper.configure(
					DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
			objectMapper.configure(
					DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);

			objectMapper
					.setPropertyNamingStrategy(new PropertyNamingStrategy.PascalCaseStrategy());

			objectMapper
					.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			objectMapper
					.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			objectMapper
					.disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);

			jsonFactory = new JsonFactory();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@SuppressWarnings("deprecation")
	public static LH_Login loginParser(String response, String parsertype) {
		LH_Login user = null;

		initObjectmapper();

		try {
			jp = jsonFactory.createJsonParser(response);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			user = objectMapper.readValue(jp, LH_Login.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// String id = user.getUser().getId();

		return user;

	}
	
	
	@SuppressWarnings("deprecation")
	public static LH_AppVersion appVersionparser(String response) {
		LH_AppVersion appVersion = null;
		initObjectmapper();

		try {
			jp = jsonFactory.createJsonParser(response);

			// Sanity check: verify that we got "Json Object":
			if (jp.nextToken() != JsonToken.START_OBJECT) {
				throw new IOException("Expected data to start with an Object");
			}

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			appVersion = objectMapper.readValue(jp, LH_AppVersion.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return appVersion;

	}
	
	@SuppressWarnings("deprecation")
	public static LH_ComponentRegistryModel componentRegisterparser(String response) {
		LH_ComponentRegistryModel componentRegistryModel = null;
		initObjectmapper();

		try {
			jp = jsonFactory.createJsonParser(response);

			// Sanity check: verify that we got "Json Object":
			if (jp.nextToken() != JsonToken.START_OBJECT) {
				throw new IOException("Expected data to start with an Object");
			}

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			componentRegistryModel = objectMapper.readValue(jp, LH_ComponentRegistryModel.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return componentRegistryModel;

	}

	@SuppressWarnings("deprecation")
	public static LH_Menu menuparser(String response) {
		LH_Menu menu = null;
		initObjectmapper();

		try {
			jp = jsonFactory.createJsonParser(response);

			// Sanity check: verify that we got "Json Object":
			if (jp.nextToken() != JsonToken.START_OBJECT) {
				throw new IOException("Expected data to start with an Object");
			}

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			menu = objectMapper.readValue(jp, LH_Menu.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return menu;

	}

	public static String readlogintoken(Activity m_context, String string) {
		sharedpreferences = m_context.getSharedPreferences(
				LH_Constants.MyPREFERENCES, Context.MODE_PRIVATE);
		String token = sharedpreferences.getString(LH_Constants.Token,
				"livehealthier");
		return token;

	}

	public static boolean saveLoginInformation(Activity m_context, LH_Login user) {

		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database = null;

		String id = "";
		String firstname = "";
		String lastname = "";
		String email = "";
		String externalid = "";

		String token = "";

		String sessionid = "";

		String clientaccountid = "";
		String clientaccountname = "";

		String officecode = "";
		String officename = "";
		String secondaryLogoPath = "";
		String mobileLogoPath = "";
		String primarycolor = "";
		String wellnessBankProgram = "";
		String officeId = "";

		String secondarycolor = "";
		try {
			id = user.getUser().get(0).getId().toString().trim();
		} catch (Exception e) {
			id = "";
			e.printStackTrace();
		}

		try {
			firstname = user.getUser().get(0).getFirstName().toString().trim();
		} catch (Exception e) {
			firstname = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			lastname = user.getUser().get(0).getLastName().toString().trim();
		} catch (Exception e) {

			lastname = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			email = user.getUser().get(0).getEmail().toString().trim();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			email = "";
			e.printStackTrace();
		}
		try {
			externalid = user.getUser().get(0).getExternalID().toString()
					.trim();
		} catch (Exception e) {
			externalid = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			token = user.getUser().get(0).getToken().toString().trim();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			token = "";
			e.printStackTrace();
		}
		try {
			officeId = user.getUser().get(0).getOfficeId().toString().trim();
		} catch (Exception e) {
			externalid = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			clientaccountid = user.getUser().get(0).getOffice().get(0)
					.getClientAccountID().toString().trim();
		} catch (Exception e) {

			clientaccountid = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			officecode = user.getUser().get(0).getOffice().get(0)
					.getOfficeCode().toString().trim();
		} catch (Exception e) {
			officecode = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			officename = user.getUser().get(0).getOffice().get(0)
					.getOfficeName().toString().trim();
		} catch (Exception e) {

			officecode = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			primarycolor = user.getUser().get(0).getOffice().get(0).getClient()
					.get(0).getPrimaryColor().toString().trim();
		} catch (Exception e) {
			primarycolor = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			secondarycolor = user.getUser().get(0).getOffice().get(0)
					.getClient().get(0).getSecondaryColor().toString().trim();
		} catch (Exception e) {
			secondarycolor = "#0099a8";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			clientaccountname = user.getUser().get(0).getOffice().get(0)
					.getClient().get(0).getClientAccountName().toString()
					.trim();
		} catch (Exception e) {
			clientaccountname = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			wellnessBankProgram = user.getUser().get(0).getOffice().get(0)
					.getClient().get(0).getWellnessProgramName().toString()
					.trim();
		} catch (Exception e) {
			wellnessBankProgram = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			secondaryLogoPath = user.getUser().get(0).getOffice().get(0)
					.getClient().get(0).getSecondaryLogoPath().toString()
					.trim();
		} catch (Exception e) {

			secondaryLogoPath = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			mobileLogoPath = user.getUser().get(0).getOffice().get(0)
					.getClient().get(0).getMobileLogo().toString().trim();
		} catch (Exception e) {

			mobileLogoPath = "";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			localStorageDBHelper = LH_User_Database.getInstance(m_context);
			database = localStorageDBHelper.getWritableDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ContentValues values = new ContentValues();

		try {
			values.put(LH_Constants.LH_ID, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._FIRSTNAME, firstname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._LASTNAME, lastname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._EMAIL, email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._EXTERNALID, externalid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._TOKEN, token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._SESSIONID, sessionid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._CLIENTACCOUNTID, clientaccountid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._OFFICEID, officeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._OFFICECODE, officecode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._OFFICENAME, officename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._PRIMARYCOLOR, primarycolor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			values.put(LH_Constants._SECONDARYCOLOR, secondarycolor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._CLIENTACCOUNTNAME, clientaccountname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._WELLNESSBANKPROGRAM, wellnessBankProgram);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SECONDARYLOGOPATH, secondaryLogoPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._MOBILELOGOURL, mobileLogoPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LH_LogHelper.logInfo(mobileLogoPath);

		try {
			database.insert(LH_User_Database.LOCALSTORAGE_TABLE_NAME, null,
					values);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	public static void savelogintoken(Activity m_context, String string) {
		sharedpreferences = m_context.getSharedPreferences(
				LH_Constants.MyPREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(LH_Constants.Token, string);
		editor.commit();
	}

	public static boolean saveMenuInformation(Activity m_context, LH_Menu menu,
			int header, int child) {
		LH_Menu_Database localStorageDBHelper;
		SQLiteDatabase database = null;

		String HasSubMenu = "";
		String SubMenu = "";
		String Title = "";
		String Type = "";
		String UniqueKey = "";

		String url = "";

		String sub_HasSubMenu = "";
		String sub_Menu = "";
		String sub_Title = "";
		String sub_Type = "";
		String sub_URL = "";

		String sub_UNIQUEKEY = "";
		try {
			localStorageDBHelper = LH_Menu_Database.getInstance(m_context);
			database = localStorageDBHelper.getWritableDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			HasSubMenu = menu.getLhmenuitemList().get(header).getHasSubMenu()
					.toString().trim();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (HasSubMenu.equalsIgnoreCase("true")) {
			try {
				try {
					sub_HasSubMenu = menu.getLhmenuitemList().get(header)
							.getSubMenu().get(child).getHasSubMenu().toString()
							.trim();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sub_Menu = null;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sub_Title = menu.getLhmenuitemList().get(header)
							.getSubMenu().get(child).getTitle().toString()
							.trim();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					// sub_Type = "webview";
					sub_Type = menu.getLhmenuitemList().get(header)
							.getSubMenu().get(child).getType().toString()
							.trim();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sub_URL = menu.getLhmenuitemList().get(header).getSubMenu()
							.get(child).getUrl().toString().trim();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					sub_UNIQUEKEY = menu.getLhmenuitemList().get(header)
							.getSubMenu().get(child).getUniqueKey().toString()
							.trim();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		try {
			UniqueKey = menu.getLhmenuitemList().get(header).getUniqueKey()
					.toString().trim();
		} catch (Exception e) {
			UniqueKey = "";
			e.printStackTrace();
		}
		try {
			Type = menu.getLhmenuitemList().get(header).getType().toString()
					.trim();
		} catch (Exception e) {
			Type = "";
			e.printStackTrace();
		}
		try {
			Title = menu.getLhmenuitemList().get(header).getTitle().toString()
					.trim();
		} catch (Exception e) {
			Title = "";
			e.printStackTrace();
		}
		try {
			url = menu.getLhmenuitemList().get(header).getUrl().toString()
					.trim();
		} catch (Exception e) {
			url = "";
			e.printStackTrace();
		}
		try {
			SubMenu = "";
		} catch (Exception e) {
			e.printStackTrace();
		}

//		LH_LogHelper.logInfo("LH_Menu--" + header + "----" + sub_HasSubMenu
//				+ "-" + sub_Menu + "-" + sub_Title + "-" + sub_Type + "-"
//				+ sub_UNIQUEKEY + "-" + url + "-" + HasSubMenu + "-" + SubMenu
//				+ "-" + Title + "-" + Type + "-" + UniqueKey + "-" + url + "-");

		ContentValues values = new ContentValues();

		try {
			values.put(LH_Constants._SUB_HASSUBMENU, sub_HasSubMenu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUB_SUBMENU, sub_Menu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUB_TITLE, sub_Title);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUB_TYPE, sub_Type);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUB_URL, sub_URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUB_UNIQUEKEY, sub_UNIQUEKEY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._HASSUBMENU, HasSubMenu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._SUBMENU, SubMenu);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._URL, url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._TITLE, Title);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._TYPE, Type);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			values.put(LH_Constants._UNIQUEKEY, UniqueKey);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			database.insert(LH_Menu_Database.LOCALSTORAGE_TABLE_NAME, null,
					values);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;

	}

	public static String getclientid(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String clientid = null;
		if (cursor.moveToFirst()) {
			clientid = cursor.getString(cursor
					.getColumnIndex("ClientAccountID"));

		}// TODO Auto-generated method stub
		return clientid;
	}

	public static String getclientName(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String clientName = null;
		if (cursor.moveToFirst()) {
			clientName = cursor.getString(cursor
					.getColumnIndex(LH_Constants._WELLNESSBANKPROGRAM));

		}// TODO Auto-generated method stub
		return clientName;
	}

	public static String getOfficeId(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String officeId = null;
		if (cursor.moveToFirst()) {
			officeId = cursor.getString(cursor
					.getColumnIndex(LH_Constants._OFFICEID));

		}// TODO Auto-generated method stub
		return officeId;
	}

	public static String getUserid(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String UserId = null;
		if (cursor.moveToFirst()) {
			UserId = cursor
					.getString(cursor.getColumnIndex(LH_Constants.LH_ID));

		}// TODO Auto-generated method stub
		return UserId;
	}

	public static String getOfficeCode(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String OfficeID = null;
		if (cursor.moveToFirst()) {
			OfficeID = cursor.getString(cursor
					.getColumnIndex(LH_Constants._OFFICECODE));

		}// TODO Auto-generated method stub
		return OfficeID;
	}

	public static String getWebToken(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String token = null;
		if (cursor.moveToFirst()) {
			token = cursor.getString(cursor.getColumnIndex("Token"));

		}// TODO Auto-generated method stub
		return token;
	}

	public static String getPrimaryColor(Activity mcontext) {
		Context mContext = mcontext;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String color = null;
		if (cursor.moveToFirst()) {
			color = cursor.getString(cursor
					.getColumnIndex(LH_Constants._PRIMARYCOLOR));

		}// TODO Auto-generated method stub
		return color;
	}

	public static String getSecondaryColor(Context _context) {
		Context mContext = _context;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String color = null;
		if (cursor.moveToFirst()) {
			color = cursor.getString(cursor
					.getColumnIndex(LH_Constants._SECONDARYCOLOR));

		}// TODO Auto-generated method stub
		return color;
	}

	public static void deleteUsertable(Context _context) {
		Context mContext = _context;
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getWritableDatabase();

		try {
			database.delete(LH_User_Database.LOCALSTORAGE_TABLE_NAME, null,
					null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// TODO Auto-generated method stub

	}

	public static void deleteMenutable(Context _context) {
		Context mContext = _context;
		LH_Menu_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_Menu_Database.getInstance(mContext);

		database = localStorageDBHelper.getWritableDatabase();

		try {
			database.delete(LH_Menu_Database.LOCALSTORAGE_TABLE_NAME, null,
					null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// TODO Auto-generated method stub

	}

	public static String getUniqueKeyForMenuItem(String title) {
		if (null != title) {
			return title.toLowerCase().replace(" ", "").replace("'", "");
		} else {
			return "";
		}
	}

}
