package com.livehealthier.mobile.utils;

import android.util.Log;

import com.livehealthier.mobile.activity.LHApplication;

public class LH_LogHelper {

	public static void logInfo(String message) {
		if (LHApplication.CurrentEnvironment.getIsVerboseLoggingEnabled()) {
			Log.v(LH_Constants.LOG_TAG, message);
		}
	}

	public static void logDebug(String message) {
		if (LHApplication.CurrentEnvironment.getIsDebugLoggingEnabled()) {
			Log.d(LH_Constants.LOG_TAG, message);
		}
	}

	public static void logWarning(String message) {
		if (LHApplication.CurrentEnvironment.getIsVerboseLoggingEnabled()) {
			Log.v(LH_Constants.LOG_TAG, message);
		}
	}

}
