package com.livehealthier.mobile.adapter;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

@SuppressLint({ "NewApi", "ResourceAsColor" })
public class ExpandableListAdapter extends BaseExpandableListAdapter {
	private static int selectedIndex;
	private static int ChildselectedIndex;
	private Context _context;
	private String secondary;
	private List<String> _listDataHeader = null; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild = null;
	private static final int[] EMPTY_STATE_SET = {};
	private static final int[] GROUP_EXPANDED_STATE_SET = { android.R.attr.state_expanded };
	private static final int[][] GROUP_STATE_SETS = { EMPTY_STATE_SET, // 0
			GROUP_EXPANDED_STATE_SET // 1
	};

	public ExpandableListAdapter(Context context, List<String> listDataHeader,
			HashMap<String, List<String>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
		secondary = LH_Utils.getSecondaryColor(_context);

		LH_LogHelper.logInfo("secondary color - " + secondary);

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		if (null != _listDataChild) {
			if (_listDataChild.get(_listDataHeader.get(groupPosition)).size() == 0) {
				return 0;
			} else {
				return _listDataChild.get(_listDataHeader.get(groupPosition))
						.size();
			}
		} else {
			return 0;
		}

	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item, null);
		}

		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.lblListItem);

		txtListChild.setText(childText);
		if (ChildselectedIndex == childPosition) {//
			// convertView.setBackgroundColor(Color.rgb(102, 204, 255));

			convertView.setBackgroundColor(Color.parseColor(secondary));
			txtListChild.setTextColor(Color.WHITE);

		} else {
			convertView.setBackgroundColor(Color.TRANSPARENT);
			txtListChild.setTextColor(Color.BLACK);
		}
		return convertView;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@SuppressLint("NewApi")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);

		View ind = convertView.findViewById(R.id.explist_indicator);
		ImageView indicator = null;
		if (ind != null) {
			indicator = (ImageView) ind;
			if (getChildrenCount(groupPosition) == 0) {
				indicator.setVisibility(View.INVISIBLE);

			} else {
				indicator.setVisibility(View.VISIBLE);
				int stateSetIndex = (isExpanded ? 1 : 0);
				Drawable drawable = indicator.getDrawable();
				drawable.setState(GROUP_STATE_SETS[stateSetIndex]);
			}
		}
		// lblListHeader.setTypeface(null, Typeface.NORMAL);
		lblListHeader.setText(headerTitle);

		if (selectedIndex == groupPosition) {//
			// convertView.setBackgroundColor(Color.rgb(102, 204, 255));

			convertView.setBackgroundColor(Color.parseColor(secondary));
			lblListHeader.setTextColor(Color.WHITE);

		} else {
			convertView.setBackgroundColor(Color.TRANSPARENT);
			lblListHeader.setTextColor(Color.BLACK);
			// indicator.setVisibility(View.INVISIBLE);
			int stateSetIndex = (isExpanded ? 1 : 0);
			Drawable drawable = indicator.getDrawable();
			drawable.setState(GROUP_STATE_SETS[stateSetIndex]);

		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public void setSelectedIndex(int groupPosition) {
		// TODO Auto-generated method stub
		selectedIndex = groupPosition;
	}

	public void setSelectedChildIndex(int groupPosition) {
		// TODO Auto-generated method stub
		ChildselectedIndex = groupPosition;
	}

}
