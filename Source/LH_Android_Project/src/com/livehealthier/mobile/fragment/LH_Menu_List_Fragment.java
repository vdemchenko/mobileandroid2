package com.livehealthier.mobile.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.activity.LH_Activity;
import com.livehealthier.mobile.adapter.ExpandableListAdapter;
import com.livehealthier.mobile.database.LH_Menu_Database;
import com.livehealthier.mobile.database.LH_User_Database;
import com.livehealthier.mobile.utils.ImageLoader;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

@SuppressLint("NewApi")
public class LH_Menu_List_Fragment extends Fragment {

	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	List<String> listUniqueID;
	HashMap<String, List<String>> listDataChild;
	HashMap<String, List<String>> listUniqueIDChild;
	TextView client_name;
	HashMap<String, List<String>> listmenucollection;
	private ImageView imgView;
	private ImageLoader imgLoader;
	private Activity activity;
	public static LH_Menu_List_Fragment LhMenuFragment;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// preparing list data
		prepareListData();
		prepareUserData();
		LhMenuFragment = this;
		activity = getActivity();
		listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader,
				listDataChild);

		// setting list adapter
		expListView.setAdapter(listAdapter);
		expListView.setGroupIndicator(null);
		expListView.setBackgroundColor(Color.TRANSPARENT);
		// expListView.setSelector(Color.TRANSPARENT);
		// setgroupindicatorright();

		if (null != LHApplication.Menu
				&& null != LHApplication.Menu.getLhmenuitemList()
				&& LHApplication.Menu.getLhmenuitemList().size() > 0) {

			setMenuSelection(LHApplication.Menu.getLhmenuitemList().get(0)
					.getTitle());
		}

		// Listview Group click listener
		expListView.setOnGroupClickListener(new OnGroupClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {

				listAdapter.setSelectedIndex(groupPosition);
				listAdapter.setSelectedChildIndex(-1);
				listAdapter.notifyDataSetChanged();
				int menusize = 0;
				if (null != LHApplication.Menu) {
					menusize = LHApplication.Menu.getLhmenuitemList().size();
				} else {
					LHApplication.LogoutUser(getActivity());
					getActivity().finish();
				}
				for (String s : listUniqueID) {

					if (s.equalsIgnoreCase(listUniqueID.get(groupPosition))) {
						for (int i = 0; i < menusize; i++) {
							if (s.equalsIgnoreCase(LHApplication.Menu
									.getLhmenuitemList().get(i).getUniqueKey())) {

								String title = LHApplication.Menu
										.getLhmenuitemList().get(i).getTitle()
										.toString();

								if (title.equalsIgnoreCase("logout")
										|| title.equalsIgnoreCase("Log Out")) {

									final AlertDialog alertDialog = new AlertDialog.Builder(
											getActivity()).create();
									alertDialog
											.setTitle(getActivity()
													.getApplicationContext()
													.getResources()
													.getString(
															R.string.logout_heading));
									alertDialog.setMessage(getActivity()
											.getApplicationContext()
											.getResources()
											.getString(R.string.logout));
									alertDialog.setCancelable(false);
									alertDialog
											.setButton(
													getActivity()
															.getApplicationContext()
															.getResources()
															.getString(
																	R.string.logout_heading),
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {

															LHApplication
																	.LogoutUser(getActivity());
															getActivity()
																	.finish();
															setGoogleAnalytics("Log Out");

														}
													});

									alertDialog
											.setButton2(
													getActivity()
															.getApplicationContext()
															.getResources()
															.getString(
																	R.string.Cancel),
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															alertDialog
																	.dismiss();
															((LH_Activity) getActivity())
																	.getSlidingMenu()
																	.toggle();
														}
													});

									alertDialog.show();
								}

								try {
									String menutype = "";

									try {
										menutype = LHApplication.Menu
												.getLhmenuitemList().get(i)
												.getType().toString();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										return false;

									}

									if (!menutype.equals(null)) {
										if ("webview"
												.equalsIgnoreCase(menutype)) {

											String weburl = LHApplication.Menu
													.getLhmenuitemList().get(i)
													.getUrl().toString();

											LH_LogHelper.logInfo(weburl);

											// Code for Webview

											Fragment homefragment = new LH_Home();
											FragmentTransaction transaction = getFragmentManager()
													.beginTransaction();

											Bundle bundle = new Bundle();
											bundle.putString("Title",
													listDataHeader
															.get(groupPosition));
											bundle.putString("url", weburl);
											homefragment.setArguments(bundle);

											// Store webUrl for future use on
											// the Home Fragment, this will be
											// useful in timeout handling
											LHApplication.SetMenuClick(weburl);

											setGoogleAnalytics(title);
											// Replace whatever is in the
											// fragment_container view with
											// this fragment,
											// and add the transaction to
											// the back stack
											transaction.replace(
													R.id.content_frame,
													homefragment);
											transaction
													.addToBackStack("livehealthier");

											// Commit the transaction
											transaction.commit();
											((LH_Activity) getActivity())
													.getSlidingMenu().toggle();

											LH_LogHelper.logInfo("Menu type - "
													+ weburl);
										} else if ("ionic"
												.equalsIgnoreCase(menutype)) {
											LH_LogHelper
													.logInfo("Menu type - ionic");
										} else if ("native"
												.equalsIgnoreCase(menutype)) {
											String weburl = LHApplication.Menu
													.getLhmenuitemList().get(i)
													.getUrl().toString()
													.toLowerCase();
											if (weburl
													.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
												Intent emailIntent = new Intent(
														Intent.ACTION_SENDTO,
														Uri.parse(weburl));
												String EmailBody = getEmailBody();
												//emailIntent.setType("message/rfc822");
												emailIntent.putExtra(Intent.EXTRA_TEXT, EmailBody);
												

											

												startActivity(emailIntent);
												LH_LogHelper
														.logInfo("Menu type - native email - "
																+ weburl);
												setGoogleAnalytics("E-mail");
											}
											LH_LogHelper
													.logInfo("Menu type - native");
										}

									} else {

										return false;
									}
								} catch (Exception e) {
									e.printStackTrace();
									return false;
								}

								return false;

							}
						}
					}

				}

				return false;
			}

		

		});

		// Listview Group expanded listener
		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {

			}
		});

		// Listview Group collasped listener
		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

			@Override
			public void onGroupCollapse(int groupPosition) {

			}
		});

		// Listview on child click listener
		expListView.setOnChildClickListener(new OnChildClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {

				listAdapter.setSelectedChildIndex(childPosition);
				listAdapter.setSelectedIndex(-1);
				listAdapter.notifyDataSetChanged();

				if (listUniqueIDChild.get(listUniqueID.get(groupPosition))
						.get(childPosition).equalsIgnoreCase("logout")) {

					final AlertDialog alertDialog = new AlertDialog.Builder(
							getActivity()).create();
					alertDialog.setTitle(getActivity().getApplicationContext()
							.getResources().getString(R.string.logout_heading));
					alertDialog.setMessage(getActivity()
							.getApplicationContext().getResources()
							.getString(R.string.logout));
					alertDialog.setCancelable(false);
					alertDialog.setButton(getActivity().getApplicationContext()
							.getResources().getString(R.string.logout_heading),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									LHApplication.LogoutUser(getActivity());
									getActivity().finish();
									setGoogleAnalytics("Log Out");
								}
							});

					alertDialog.setButton2(
							getActivity().getApplicationContext()
									.getResources().getString(R.string.Cancel),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									alertDialog.dismiss();
									((LH_Activity) getActivity())
											.getSlidingMenu().toggle();
								}
							});

					alertDialog.show();
				} else {

					for (Map.Entry<String, List<String>> entry : listUniqueIDChild
							.entrySet()) {

						String key = entry.getKey();

						List<String> values = entry.getValue();

						for (String s : values) {

							String childkey = listUniqueIDChild.get(
									listUniqueID.get(groupPosition)).get(
									childPosition);

							if (s.equalsIgnoreCase(listUniqueIDChild.get(
									listUniqueID.get(groupPosition)).get(
									childPosition))) {

								// for (int i = 0;i<menusize ; i++) {
								if (s.equalsIgnoreCase(LHApplication.Menu
										.getLhmenuitemList().get(groupPosition)
										.getSubMenu().get(childPosition)
										.getUniqueKey())) {
									try {
										String menutype = "";
										String title = "";
										try {
											title = LHApplication.Menu
													.getLhmenuitemList()
													.get(groupPosition)
													.getSubMenu()
													.get(childPosition)
													.getTitle();
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}

										try {
											menutype = LHApplication.Menu
													.getLhmenuitemList()
													.get(groupPosition)
													.getSubMenu()
													.get(childPosition)
													.getType().toString();
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
											return false;
										}

										if ("webview"
												.equalsIgnoreCase(menutype)) {

											String weburl = LHApplication.Menu
													.getLhmenuitemList()
													.get(groupPosition)
													.getSubMenu()
													.get(childPosition)
													.getUrl().toString();

											LH_LogHelper.logInfo(weburl);

											// Code for Webview

											Fragment homefragment = new LH_Home();
											FragmentTransaction transaction = getFragmentManager()
													.beginTransaction();

											Bundle bundle = new Bundle();
											bundle.putString(
													"Title",
													listDataChild
															.get(listDataHeader
																	.get(groupPosition))
															.get(childPosition));
											bundle.putString("url", weburl);
											homefragment.setArguments(bundle);

											// Store webUrl for future use on
											// the Home Fragment, this will be
											// useful in timeout handling
											LHApplication.SetMenuClick(weburl);
											setGoogleAnalytics(title);
											// Replace whatever is in the
											// fragment_container view with
											// this fragment,
											// and add the transaction to the
											// back stack
											transaction.replace(
													R.id.content_frame,
													homefragment);
											transaction
													.addToBackStack("livehealthier");

											// Commit the transaction
											transaction.commit();
											((LH_Activity) getActivity())
													.getSlidingMenu().toggle();

											LH_LogHelper.logInfo("Menu type - "
													+ weburl);
										} else if ("ionic"
												.equalsIgnoreCase(menutype)) {
											LH_LogHelper
													.logInfo("Menu type - ionic");
										} else if ("native"
												.equalsIgnoreCase(menutype)) {
											String weburl = LHApplication.Menu
													.getLhmenuitemList()
													.get(groupPosition)
													.getSubMenu()
													.get(childPosition)
													.getUrl().toString()
													.toLowerCase();
											if (weburl
													.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
												Intent emailIntent = new Intent(
														Intent.ACTION_SENDTO,
														Uri.parse(weburl));
												String EmailBody = getEmailBody();
											//	emailIntent.setType("message/rfc822");
												emailIntent.putExtra(Intent.EXTRA_TEXT, EmailBody);
												

											

												startActivity(emailIntent);
												
												
												
												setGoogleAnalytics("E-mail");
												LH_LogHelper
														.logInfo("Menu type - native email - "
																+ weburl);
											}
											LH_LogHelper
													.logInfo("Menu type - native");
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									return false;
								}

							}
						}
					}

				}
				return false;
			}
		});

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.list, null);
		client_name = (TextView) v.findViewById(R.id.textView1);
		expListView = (ExpandableListView) v.findViewById(R.id.lvExp);
		imgView = (ImageView) v.findViewById(R.id.logo);
		imgLoader = new ImageLoader(getActivity());
		return v;
	}

	private void prepareListData() {

		Context mContext = getActivity().getApplicationContext();
		LH_Menu_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_Menu_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT  * FROM "
				+ LH_Menu_Database.LOCALSTORAGE_TABLE_NAME;

		Cursor cursor = database.rawQuery(selectQuery, null);

		listDataHeader = new ArrayList<String>();
		listUniqueID = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();

		listUniqueIDChild = new HashMap<String, List<String>>();

		Map<String, List<String>> tempsubmenu = new HashMap<String, List<String>>();

		Map<String, List<String>> tempuniqueidmap = new HashMap<String, List<String>>();

		List<String> submenu = new ArrayList<String>();
		List<String> submenuuniqueid = new ArrayList<String>();
		String submenuname = "";
		String Uniqueid_submenu = "";
		if (cursor.moveToFirst()) {
			do {
				String menu = "";
				String name = "";
				String Uniqueid_Header = "";

				name = cursor.getString(cursor.getColumnIndex("Title"));
				Uniqueid_Header = cursor.getString(cursor
						.getColumnIndex("UniqueKey"));
				menu = cursor.getString(cursor.getColumnIndex("HasSubMenu"));
				if (menu.equalsIgnoreCase("true")) {

					submenuname = cursor.getString(cursor
							.getColumnIndex("sub_Title"));

					Uniqueid_submenu = cursor.getString(cursor
							.getColumnIndex("sub_UniqueKey"));

					if (!submenu.contains(submenuname)) {
						submenuuniqueid.add(Uniqueid_submenu);
						tempuniqueidmap.put(Uniqueid_Header, submenuuniqueid);
						submenu.add(submenuname);
						tempsubmenu.put(name, submenu);
					}

					if (!listDataHeader.contains(name)) {

						try {
							listDataHeader.add(name);

							listUniqueID.add(Uniqueid_Header);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					// title = name
					// url =

				} else {
					// if (!listDataHeader.contains(name)) {
					List<String> notsubmenu = new ArrayList<String>();
					try {
						listDataHeader.add(name);
						listUniqueID.add(Uniqueid_Header);
						try {
							listDataChild.put(
									listDataHeader.get(cursor.getPosition()),
									notsubmenu);

							listUniqueIDChild.put(
									listUniqueID.get(cursor.getPosition()),
									notsubmenu);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// }

				}

			} while (cursor.moveToNext());
		}
		for (Map.Entry<String, List<String>> entry : tempsubmenu.entrySet()) {

			String key = entry.getKey();

			List<String> values = entry.getValue();
			listDataChild.put(listDataHeader.get(listDataHeader.indexOf(key)),
					values);

		}

		for (Map.Entry<String, List<String>> entry : tempuniqueidmap.entrySet()) {

			String key = entry.getKey();

			List<String> values = entry.getValue();
			listUniqueIDChild.put(listUniqueID.get(listUniqueID.indexOf(key)),
					values);

		}

		LH_LogHelper.logInfo("Download Complete List");
	}

	private void prepareUserData() {
		Context mContext = getActivity().getApplicationContext();
		LH_User_Database localStorageDBHelper;
		SQLiteDatabase database;

		localStorageDBHelper = LH_User_Database.getInstance(mContext);

		database = localStorageDBHelper.getReadableDatabase();

		String selectQuery = "SELECT * FROM "
				+ LH_User_Database.LOCALSTORAGE_TABLE_NAME;
		Cursor cursor = database.rawQuery(selectQuery, null);
		String name;
		String clientLogoUrl;
		if (cursor.moveToFirst()) {
			name = cursor.getString(cursor
					.getColumnIndex(LH_Constants._WELLNESSBANKPROGRAM));

			name = name.replace("&#8480;", "\u2120");
			clientLogoUrl = cursor.getString(cursor
					.getColumnIndex("MobileLogo"));
			LH_LogHelper.logInfo("name - " + name);
			if (name.length() > 1) {
				client_name.setText(name);
			} else {
				client_name.setText("Live Healthier");
			}

			LH_LogHelper.logInfo("Client LOGO - " + clientLogoUrl);
			imgLoader.DisplayImage(clientLogoUrl, imgView);
		}
	}

	public void setMenuSelection(String title) {
		if (title.length() <= 0) {
			return;
		}

		int groupPos = -1;
		int childPos = -1;
		String titleToUse = LH_Utils.getUniqueKeyForMenuItem(title);
		for (int counter = 0; counter < listUniqueID.size(); counter++) {
			if (listUniqueID.get(counter).equalsIgnoreCase(titleToUse)) {
				groupPos = counter;
				break;
			}

			if (groupPos < 0) {
				for (int childCounter = 0; childCounter < listUniqueIDChild
						.size(); childCounter++) {
					if (listUniqueIDChild.get(listUniqueID.get(counter)).size() > 0
							&& listUniqueIDChild.get(listUniqueID.get(counter))
									.get(childCounter)
									.equalsIgnoreCase(titleToUse)) {
						childPos = childCounter;
						break;
					}
				}
			}

		}

		if (groupPos > -1) {
			listAdapter.setSelectedIndex(groupPos);
			listAdapter.setSelectedChildIndex(-1);
			listAdapter.notifyDataSetChanged();
		} else if (childPos > -1) {
			listAdapter.setSelectedIndex(-1);
			listAdapter.setSelectedChildIndex(childPos);
			listAdapter.notifyDataSetChanged();
		}

	}

	private void setGoogleAnalytics(String title) {
		// TODO Auto-generated method stub

		String clientName = LH_Utils.getclientName(getActivity());
		LHApplication.analyticsHandler.eventAnalyticsHits(
				getActivity().getApplicationContext(),
				title,
				getActivity().getApplicationContext().getResources()
						.getString(R.string.GA_Category),
				getActivity().getApplicationContext().getResources()
						.getString(R.string.GA_Action_menu_item_pressed),
				title, clientName);
	}
	
	public String getEmailBody() {

		String AppVersion = "";

		try {
			AppVersion = getActivity()
					.getApplicationContext()
					.getPackageManager()
					.getPackageInfo(
							getActivity().getApplicationContext()
									.getPackageName(), 0).versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (null == AppVersion) {
			AppVersion = getActivity().getApplicationContext()
					.getString(R.string.Versionname);

		}

		String apiLevel = android.os.Build.VERSION.RELEASE;

		String DeviceModel = android.os.Build.MODEL;

		String clientName = LH_Utils.getclientName(getActivity());

		String Connection;

		ConnectivityManager connec = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		android.net.NetworkInfo wifi = connec
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		TelephonyManager mTelephonyManager = (TelephonyManager) getActivity()
				.getApplicationContext().getSystemService(
						Context.TELEPHONY_SERVICE);
	

		int networkType = mTelephonyManager.getNetworkType();
		 if (wifi.isConnected()) 
         {

			 Connection = "WIFI";

         }else{
		if (networkType == (TelephonyManager.NETWORK_TYPE_GPRS)) {
			Connection = "2G";

		} else if (networkType == TelephonyManager.NETWORK_TYPE_UMTS) {

			Connection = "3G";
		} else if (networkType == TelephonyManager.NETWORK_TYPE_LTE) {
			Connection = "LTE";

		} else {
			android.net.NetworkInfo mobile = connec
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			
			if(mobile.isConnected())
            {
				Connection = "Mobile";
            }else{
			Connection = "Unknown";
            }
		}
         }

	String 	AppName = getActivity().getApplicationContext()
				.getString(R.string.app_name);
		// TODO Auto-generated method stub
		
String EmailBody="App Version :"+ AppVersion+"\nAndroid Version :"+apiLevel +"\nDevice Model :"+DeviceModel+"\nClient :"+clientName+"\nConnection :"+Connection+"\n"+AppName+" App"+"\n"+"\n"+"\n\n\n";
	
	

	return EmailBody;
	}

}
