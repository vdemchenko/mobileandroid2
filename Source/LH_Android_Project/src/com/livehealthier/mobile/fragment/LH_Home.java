package com.livehealthier.mobile.fragment;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistryMap;
import com.livehealthier.mobile.http.LHConnectionDetector;
import com.livehealthier.mobile.utils.LH_Constants.FeatureType;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

@SuppressWarnings("deprecation")
@SuppressLint({ "NewApi", "InflateParams", "SetJavaScriptEnabled" })
public class LH_Home extends Fragment {

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {

			super.onPageFinished(view, url);
			loadingimg.setVisibility(View.INVISIBLE);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {

			loadingimg.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@JavascriptInterface
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			Map<String, String> headers = null;
			LH_LogHelper.logInfo("url - " + url);
			CookieSyncManager cookieSyncManager = CookieSyncManager
					.createInstance(LHApplication.LHApplicationContext);
			String cookies = CookieManager.getInstance().getCookie(url);
			LH_LogHelper.logInfo("cookies - " + cookies);
			cookieSyncManager.sync();

			// LH_Menu_List_Fragment.Lh;
			// check for overrides here
			LH_ComponentRegistryMap destinationOverrideComponent = null;
			destinationOverrideComponent = LHApplication.ComponentRegistry
					.getComponent(url);
			String urlToLoad = "";
			if (null != destinationOverrideComponent) {
				urlToLoad = pushComponent(destinationOverrideComponent, false);
				headers = setCookiesAndHeaders(urlToLoad);
				view.loadUrl(urlToLoad, headers);
				return true;
			}

			destinationOverrideComponent = LHApplication.ComponentRegistry
					.getComponentIfRedirectedToLogin(url);
			if (null != destinationOverrideComponent) {
				urlToLoad = pushComponent(destinationOverrideComponent, true);

				headers = setCookiesAndHeaders(urlToLoad);
				view.loadUrl(urlToLoad, headers);
				return true;
			}

			destinationOverrideComponent = LHApplication.ComponentRegistry
					.getComponentEmailRedirect(url);
			if (null != destinationOverrideComponent) {
				urlToLoad = pushComponent(destinationOverrideComponent, true);

				return true;
			}

			destinationOverrideComponent = LHApplication.ComponentRegistry
					.getComponentIfRedirectedToLoginMoreThanTwoTimes(url);
			if (null != destinationOverrideComponent) {
				urlToLoad = pushComponent(destinationOverrideComponent, true);

				return true;
			} else {
				headers = setCookiesAndHeaders(url);
				
			
				view.loadUrl(url, headers);

				return true;
			}
		}

		String pushComponent(LH_ComponentRegistryMap component,
				boolean renewSession) {

			String urlToReturn = "";
			if (component.getType().equalsIgnoreCase(FeatureType.WEB_VIEW)) {
				urlToReturn = component.getDestinationFullUrl();
				if (renewSession) {
					urlToReturn = urlToReturn + "&renewSessionRequested=true";
				} else {
					_context.getActionBar().setTitle(component.getTitle());
					LH_Menu_List_Fragment.LhMenuFragment
							.setMenuSelection(component.getTitle());
					LHApplication.ComponentRegistry
							.setLastUserInteractionItem(urlToReturn);
					setGoogleAnalytics();
				}
			} else if (component.getType().equalsIgnoreCase(FeatureType.NATIVE)) {
				if (component.getTitle().equalsIgnoreCase("loginhome")) {
					// take user to login screen after logging out the user
					if (null != _context) {
						LHApplication.LogoutUser(_context);
						_context.finish();

					}
				} else if (component.getTitle().equalsIgnoreCase("Email")) {
					// take user to login screen after logging out the user
					if (null != _context) {
						Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
								Uri.parse(component.getDestinationFullUrl()));
						startActivity(emailIntent);

					}
				}
			}

			return urlToReturn;
		}

	
		@JavascriptInterface
		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {

			loadingimg.setVisibility(View.INVISIBLE);

			final AlertDialog alertDialog = new AlertDialog.Builder(
					getActivity()).create();
			alertDialog.setTitle(getActivity().getResources().getString(
					R.string.Service_heading));
			alertDialog.setMessage(getActivity().getResources().getString(
					R.string.Service_msg));
			alertDialog.setCancelable(false);
			alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					alertDialog.cancel();

				}
			});

			alertDialog.show();

		}

	}

	private WebView homewebview;
	private WebView loadingimg;
	private Activity _context;
	// private ProgressBar progress;

	String Url;

	@JavascriptInterface
	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		_context = getActivity();
		Bundle args = getArguments();
		final LHConnectionDetector LH = new LHConnectionDetector(getActivity());
		CookieManager cookieManager = CookieManager.getInstance();
		// cookieManager.removeAllCookie();
		// cookieManager.removeExpiredCookie();
		// cookieManager.removeSessionCookie();
		if (args != null) {
			try {
				String Title = getArguments().getString(LH_Constants._TITLE);

				if (null == Title) {
					if (null != LHApplication.Menu) {

						Title = LHApplication.Menu.getLhmenuitemList().get(0)
								.getTitle();
					}
				}

				getActivity().getActionBar().setTitle(Title);

				Url = getArguments().getString(LH_Constants._URL);

				Log.v("HOME URL", Url);
				
				if (Url == null) {
					if (null != LHApplication.Menu) {
						Url = LHApplication.Menu.getLhmenuitemList().get(0)
								.getUrl().toString();
					}
				}
			
				try {
					LH_Menu_List_Fragment.LhMenuFragment
					.setMenuSelection(Title);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (Exception e) {
				getActivity().getActionBar().setTitle(
						LHApplication.Menu.getLhmenuitemList().get(0)
								.getTitle());
				Url = LHApplication.Menu.getLhmenuitemList().get(0).getUrl()
						.toString();
				e.printStackTrace();
			}
		} else {
			try {

				if (null != LHApplication.Menu) {
					getActivity().getActionBar().setTitle(
							LHApplication.Menu.getLhmenuitemList().get(0)
									.getTitle());
					Url = LHApplication.Menu.getLhmenuitemList().get(0)
							.getUrl().toString();
				}
				// Store webUrl for future use on the Home Fragment, this will
				// be useful in timeout handling
				if (null != LHApplication.Url) {
					if (LHApplication.Url.length() > 0) {
						LHApplication.SetMenuClick(Url);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
		
		
		View view = inflater.inflate(R.layout.layout_fragment, null);

		homewebview = (WebView) view.findViewById(R.id.homewebview);

		loadingimg = (WebView) view.findViewById(R.id.loadingimg);
		// loadingimg.loadUrl("file:///android_asset/loader.gif");
		loadingimg.setBackgroundColor(Color.TRANSPARENT);
		loadingimg.loadUrl("file:///android_asset/loader_generic.gif");

		setGoogleAnalytics();

		//
		// loadingimg.setBackgroundResource(R.drawable.lh_loadinganimation);
		//
		// AnimationDrawable frameAnimation = (AnimationDrawable)
		// loadingimg.getBackground();
		//
		// frameAnimation.start();

		homewebview.getSettings().setJavaScriptEnabled(true);
		homewebview.setFitsSystemWindows(true);
		homewebview.getSettings().setLoadWithOverviewMode(true);
		homewebview.getSettings().setUseWideViewPort(true);
		homewebview.getSettings().setAllowUniversalAccessFromFileURLs(false);
		homewebview.getSettings().setAllowContentAccess(true);
		homewebview.getSettings().setLoadsImagesAutomatically(true);
		homewebview.getSettings().setLoadWithOverviewMode(true);
		homewebview.addStatesFromChildren();
		homewebview.buildDrawingCache();
		homewebview.buildLayer();
		homewebview.dispatchSetActivated(true);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
		String appCachePath = getActivity().getApplicationContext()
				.getCacheDir().getAbsolutePath();

		homewebview.getSettings().setAppCachePath(appCachePath);
		homewebview.getSettings().setDomStorageEnabled(true);
		homewebview.getSettings().setAppCacheEnabled(true);
		homewebview.getSettings().setCacheMode(				WebSettings.LOAD_CACHE_ELSE_NETWORK);
		
		 homewebview.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
		
		}
		
		
		homewebview.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					WebView webView = (WebView) v;

					switch (keyCode) {
					case KeyEvent.KEYCODE_BACK:
						if (webView.canGoBack()) {
							if (isHomePage(webView)) {
								_context.finish();
							} else {

								adjustMenuAndTitleForWebView(webView);
								webView.goBack();

								LH_LogHelper.logInfo("title - "
										+ getActivity().getActionBar()
												.getTitle());
								return true;
							}
						}
						break;
					}
				}

				return false;
			}
		});
		  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			  homewebview.setWebContentsDebuggingEnabled(true);
		  }
		homewebview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		homewebview.setScrollbarFadingEnabled(false);
//		String token = LH_Utils.getWebToken(getActivity());
//
//		String PrimaryColor = LH_Utils.getPrimaryColor(getActivity());
//		String SecondaryColor = LH_Utils.getSecondaryColor(getActivity());
//
//		String OfficeCode = LH_Utils.getOfficeCode(getActivity());
//		String UserID = LH_Utils.getUserid(getActivity());
//
//		String clientID = LH_Utils.getclientid(getActivity());
		Map<String, String> headers = null;
		if (null != Url && Url.length() > 1) {
			try {
				headers = setCookiesAndHeaders(Url);
				// new cookies add for color code

//				cookieManager.setCookie(Url, "MobileUserAuthHandle=" + token);
//				cookieManager.setCookie(Url, "PrimaryColor=" + PrimaryColor);
//				cookieManager
//						.setCookie(Url, "SecondaryColor=" + SecondaryColor);
//				cookieManager.setCookie(Url, "OfficeCode=" + OfficeCode);
//				cookieManager.setCookie(Url, "UserId=" + UserID);
//				cookieManager.setCookie(Url, "ClientAccountCode=" + clientID);
//				cookieManager.setCookie(Url, "LHApplicationNative=true");
				// cookieSyncManager.sync();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

//		LH_LogHelper.logInfo("token - " + token + "" + PrimaryColor + " "
//				+ SecondaryColor + " " + OfficeCode + " " + UserID + " "
//				+ clientID);

		
//		extraHeaders.put("MobileUserAuthHandle", token);
//		extraHeaders.put("PrimaryColor", PrimaryColor);
//		extraHeaders.put("SecondaryColor", SecondaryColor);
//		extraHeaders.put("OfficeCode", OfficeCode);
//		extraHeaders.put("UserId", UserID);
//		extraHeaders.put("ClientAccountCode", clientID);
//		extraHeaders.put("LHApplicationNative", "true");

		if (LH.isConnectingToInternet()) {
			homewebview.loadUrl(Url, headers);
			Log.v("HOME URL - 2", Url);
		} else {

			final AlertDialog alertDialog = new AlertDialog.Builder(
					getActivity()).create();
			alertDialog.setTitle(getActivity().getResources().getString(
					R.string.Network_heading));
			alertDialog.setMessage(getActivity().getResources().getString(
					R.string.Network_msg));
			alertDialog.setCancelable(false);
			alertDialog.setButton(
					getActivity().getResources().getString(
							R.string.Network_setting),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							try {
								startActivity(new Intent(
										Settings.ACTION_SETTINGS));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});

			alertDialog.setButton2(
					getActivity().getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							alertDialog.cancel();

						}
					});

			// alertDialog.setIc

			alertDialog.show();

		}
		// progress = (ProgressBar) view.findViewById(R.id.progressBar2);
		// progress.setMax(100);
		homewebview.setWebViewClient(new MyWebViewClient());

		return view;
	}

	
	Map<String, String> setCookiesAndHeaders(String urlRequested) {
		
		Map<String, String> extraHeaders = new HashMap<String, String>();
		if (null == urlRequested) {
			return extraHeaders;
		}
		CookieManager cookieManager = CookieManager.getInstance();

		String token = LH_Utils.getWebToken(_context);

		String PrimaryColor = LH_Utils.getPrimaryColor(_context);
		String SecondaryColor = LH_Utils.getSecondaryColor(_context);

		String OfficeCode = LH_Utils.getOfficeCode(_context);
		String UserID = LH_Utils.getUserid(_context);
		String ClientName = LH_Utils.getclientName(_context);
		String clientID = LH_Utils.getclientid(_context);
		cookieManager.setCookie(urlRequested, "MobileUserAuthHandle="
				+ token);
		cookieManager.setCookie(urlRequested, "PrimaryColor="
				+ PrimaryColor);
		cookieManager.setCookie(urlRequested, "SecondaryColor="
				+ SecondaryColor);
		cookieManager.setCookie(urlRequested, "OfficeCode=" + OfficeCode);
		cookieManager.setCookie(urlRequested, "UserId=" + UserID);
		cookieManager.setCookie(urlRequested, "ClientAccountCode="
				+ clientID);
		cookieManager.setCookie(urlRequested, "LHApplicationNative=true");
		cookieManager.setCookie(urlRequested, "Client="+ClientName);
		
		String AppVersion="";
		try {
			AppVersion = getActivity().getApplicationContext().getPackageManager()
				    .getPackageInfo(getActivity().getApplicationContext().getPackageName(), 0).versionName;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 if(null!=AppVersion){
		 
		cookieManager.setCookie(urlRequested, "AppVersion=" + AppVersion);
		 }else{
			 cookieManager.setCookie(urlRequested, "AppVersion=" + getActivity().getApplicationContext().getString(R.string.Versionname));
		 }
		cookieManager.setCookie(urlRequested, "Agent=Android");
		cookieManager.setCookie(urlRequested, "MobileAppContext=true");
		
		LH_LogHelper.logInfo("AppVersion - " + AppVersion );
		
		extraHeaders.put("MobileUserAuthHandle", token);
		extraHeaders.put("PrimaryColor", PrimaryColor);
		extraHeaders.put("SecondaryColor", SecondaryColor);
		extraHeaders.put("OfficeCode", OfficeCode);
		extraHeaders.put("UserId", UserID);
		extraHeaders.put("ClientAccountCode", clientID);
		extraHeaders.put("LHApplicationNative", "true");
		extraHeaders.put("AppVersion", AppVersion);
		extraHeaders.put("Client", ClientName);
		extraHeaders.put("Agent", "Android");
		extraHeaders.put("MobileAppContext", "true");
		return extraHeaders;
	}

	
	private void setGoogleAnalytics() {

		try {
			String screenName = getActivity().getActionBar().getTitle().toString()
					.trim();

			String clientName = LH_Utils.getclientName(_context);
			LHApplication.analyticsHandler.screenAnalyticsHits(getActivity()
					.getApplicationContext(), screenName, clientName);// TODO
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
																	// Auto-generated
		// method stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public void setValue(int progress) {
		// this.progress.setProgress(progress);
	}

	private boolean isHomePage(WebView view) {
		boolean onHomePage = false;

		try {
			WebBackForwardList webViewHistory = view.copyBackForwardList();
			if (null != webViewHistory) {
				int currentIndex = webViewHistory.getCurrentIndex();
				if (currentIndex >= 0) {
					WebHistoryItem item = webViewHistory
							.getItemAtIndex(currentIndex);
					String currentTitle = LHApplication.ComponentRegistry
							.getTitleForUrl(item.getUrl());
					if (currentTitle.equalsIgnoreCase(LHApplication.Menu
							.getLhmenuitemList().get(0).getTitle())) {
						onHomePage = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return onHomePage;
	}

	private void adjustMenuAndTitleForWebView(WebView view) {
		try {

			WebBackForwardList webViewHistory = view.copyBackForwardList();
			if (null != webViewHistory) {
				int currentIndex = webViewHistory.getCurrentIndex();
				if (currentIndex > 0) {
					WebHistoryItem item = webViewHistory
							.getItemAtIndex(currentIndex - 1);
					String prevUrl = item.getUrl();
					LH_ComponentRegistryMap prevComponent = LHApplication.ComponentRegistry
							.getComponent(prevUrl);
					if (null != prevComponent) {
						String prevTitle = LHApplication.ComponentRegistry
								.getPreviousAccessedComponentTitle();
						if (prevTitle
								.equalsIgnoreCase(prevComponent.getTitle())) {
							getActivity().getActionBar().setTitle(prevTitle);
							LH_Menu_List_Fragment.LhMenuFragment
									.setMenuSelection(prevTitle);
							if (null != LHApplication.Menu
									&& prevTitle
											.equalsIgnoreCase(LHApplication.Menu
													.getLhmenuitemList().get(0)
													.getTitle())) {
								LHApplication.ComponentRegistry
										.resetLastAccessComponentStack();
								LHApplication.ComponentRegistry
										.setLastUserInteractionItem(prevTitle);
							} else {
								LHApplication.ComponentRegistry
										.setLastUserInteractionItemOnBackClick(prevTitle);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
