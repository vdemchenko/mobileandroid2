package com.livehealthier.mobile.component_registry;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

import android.content.Context;
import android.content.SharedPreferences;

import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.activity.LHSplashScreen;
import com.livehealthier.mobile.api.LHComponentRegistryAPI;
import com.livehealthier.mobile.model.LH_ComponentRegistryItems;
import com.livehealthier.mobile.model.LH_MenuItems;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Constants.FeatureType;
import com.livehealthier.mobile.utils.LH_Constants.SpecialPages;

public class LH_ComponentRegistry {

	private static final String SHARED_PREFS_FILE = null;
	private static ArrayList<LH_ComponentRegistryMap> _componentRegistryMap;
	private String _title;
	private static LH_ComponentRegistryMap _lastAccessComponentRegistryMap;
	private static Stack<LH_ComponentRegistryMap> _componentStack;

	public LH_ComponentRegistry() {
		// _componentStack = new Stack<LH_ComponentRegistryMap>();

		if (null == _componentRegistryMap) {
			_componentRegistryMap = new ArrayList<LH_ComponentRegistryMap>();

		} else {
			// _componentRegistryMap.clear();
		}
		_componentStack = new Stack<LH_ComponentRegistryMap>();
	}

	public void initializeRegistry() {

		// if (null == _componentRegistryMap) {
		// _componentRegistryMap = new ArrayList<LH_ComponentRegistryMap>();
		//
		//
		//
		// } else {
		// _componentRegistryMap.clear();
		// }

		//_componentStack = new Stack<LH_ComponentRegistryMap>();
		// _componentRegistryMap =_componentRegistryMap;
	
		
		_componentRegistryMap.addAll(LHSplashScreen.ReadComponentRegistry());

		// if(null==_componentRegistryMap){
		//
		// }
//		 LH_ComponentRegistryMap componentRegistryMap1 = new
//		 LH_ComponentRegistryMap();
//
//		 List<String> parturls = new ArrayList<>();
//			parturls.add("/v2/index.aspx?challengeId=108");
//			componentRegistryMap1.setParturls(parturls);
//		 
//		
//		 componentRegistryMap1.setType(FeatureType.WEB_VIEW);
//		 _componentRegistryMap.add(componentRegistryMap1);
		//
		//
		//
		// LH_ComponentRegistryMap componentRegistryMap3 = new
		// LH_ComponentRegistryMap();
		// componentRegistryMap3.setKeyPartUrl("/V2/index.aspx");
		// componentRegistryMap3.setDestinationPartUrl("/homeApp/home");
		// componentRegistryMap3.setType(FeatureType.WEB_VIEW);
		// _componentRegistryMap.add(componentRegistryMap3);

		// LH_ComponentRegistryMap componentRegistryMap2 = new
		// LH_ComponentRegistryMap();
		// componentRegistryMap2
		// .setKeyPartUrl("/V2/MemberServices/WellnessIncentives/Index.aspx");
		// componentRegistryMap2.setDestinationPartUrl("wbApp/wellnessBank");
		// componentRegistryMap2.setType(FeatureType.WEB_VIEW);
		// _componentRegistryMap.add(componentRegistryMap2);
		//
		//
		//
		//
		// LH_ComponentRegistryMap componentRegistryMap4 = new
		// LH_ComponentRegistryMap();
		// componentRegistryMap4.setKeyPartUrl("/V2/Terms.aspx");
		// componentRegistryMap4.setDestinationPartUrl("/V2/Terms.aspx");
		// componentRegistryMap4.setType(FeatureType.WEB_VIEW);
		// _componentRegistryMap.add(componentRegistryMap4);
		//
		// LH_ComponentRegistryMap componentRegistryMap5 = new
		// LH_ComponentRegistryMap();
		// componentRegistryMap5.setKeyPartUrl("/V2/PrivacyPolicy.aspx");
		// componentRegistryMap5.setDestinationPartUrl("/V2/PrivacyPolicy.aspx");
		// componentRegistryMap5.setType(FeatureType.WEB_VIEW);
		// _componentRegistryMap.add(componentRegistryMap5);
		//
		// LH_ComponentRegistryMap componentRegistryMap6 = new
		// LH_ComponentRegistryMap();
		// componentRegistryMap6.setKeyPartUrl("/V2/MemberServices/BYOA/Store.aspx");
		// componentRegistryMap6.setDestinationPartUrl("/V2/MemberServices/BYOA/Store.aspx");
		// componentRegistryMap6.setType(FeatureType.WEB_VIEW);
		// _componentRegistryMap.add(componentRegistryMap6);

		/*
		 * /V2/MemberServices/MSEvents/HealthAssessmentWelcome.aspx
		 * /V2/MemberServices/MSEvents/HealthAssessmentWelcome.aspx
		 * /V2/MemberServices/WellnessIncentives/Index.aspx wbApp/wellnessBank
		 * /V2/index.aspx /homeApp/home /V2/Terms.aspx /V2/Terms.aspx
		 * /V2/PrivacyPolicy.aspx /V2/PrivacyPolicy.aspx
		 * /default.aspx?challengeId= /default.aspx?challengeId=
		 */

	}

	public boolean setTitle(String url, String title) {
		boolean itemFound = false;

		if (null == url || title == null) {
			return false;
		}

		String urlToUse = url.toLowerCase();
		String titleToUse = title.toLowerCase();

		if (urlToUse.contains("log out") || urlToUse.contains("logout")
				|| titleToUse.contains("logout")
				|| titleToUse.contains("log out")) {
			return false;
		}

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			// String urlPart =
			// componentRM.getDestinationPartUrl().toLowerCase();
			for (String urlPart : componentRM.getParturls()) {
				if (urlToUse.contains(urlPart)) {
					componentRM.setTitle(title);
					componentRM.setDestinationFullUrl(url);
					itemFound = true;
					break;
				}
			}

		}

		if (!itemFound) {
			if (urlToUse.contains(SpecialPages.PORTAL_MOBILE_AUTO_LOGIN_PAGE)) {
				int startPos = urlToUse
						.indexOf(SpecialPages.PORTAL_MOBILE_AUTO_LOGIN_PAGE);
				String partUrl = urlToUse.substring(startPos
						+ SpecialPages.PORTAL_MOBILE_AUTO_LOGIN_PAGE.length());
				List<String> parturls = new ArrayList<>();
				parturls.add(partUrl);
				LH_ComponentRegistryMap componentRegistryMap = new LH_ComponentRegistryMap();
				componentRegistryMap.setTitle(title);
				componentRegistryMap.setParturls(parturls);
				// componentRegistryMap.setDestinationPartUrl(partUrl);
				componentRegistryMap.setDestinationFullUrl(url);
				componentRegistryMap.setType(FeatureType.WEB_VIEW);
				componentRegistryMap.set_isMenu(true);
				LH_ComponentRegistry._componentRegistryMap
						.add(componentRegistryMap);
			}

			else if (urlToUse.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
				LH_ComponentRegistryMap emailComponentRegistryMap = new LH_ComponentRegistryMap();
				List<String> parturls = new ArrayList<>();
				parturls.add(urlToUse);
				emailComponentRegistryMap.set_isMenu(true);
				emailComponentRegistryMap.setParturls(parturls);
				// emailComponentRegistryMap.setDestinationPartUrl(urlToUse);
				emailComponentRegistryMap.setDestinationFullUrl(urlToUse);
				emailComponentRegistryMap.setTitle(title);
				emailComponentRegistryMap.setType(FeatureType.NATIVE);
				// this._componentRegistryMap.add(emailComponentRegistryMap);
			}else{
				
				List<String> parturls = new ArrayList<>();
				parturls.add(urlToUse);
				LH_ComponentRegistryMap componentRegistryMap = new LH_ComponentRegistryMap();
				componentRegistryMap.setTitle(title);
				componentRegistryMap.setParturls(parturls);
				// componentRegistryMap.setDestinationPartUrl(partUrl);
				componentRegistryMap.setDestinationFullUrl(url);
				componentRegistryMap.setType(FeatureType.WEB_VIEW);
				componentRegistryMap.set_isMenu(true);
				LH_ComponentRegistry._componentRegistryMap
						.add(componentRegistryMap);
			}
		}

		return itemFound;
	}

	public boolean setLastUserInteractionItem(String destinationUrl) {
		if (null == destinationUrl) {
			return false;
		}
		boolean itemFound = false;
		String urlToUse = destinationUrl.toLowerCase();

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			// String urlPart =
			// componentRM.getDestinationPartUrl().toLowerCase();

			for (String urlPart : componentRM.getParturls()) {
				if (urlToUse.contains(urlPart)) {
					LH_ComponentRegistry._lastAccessComponentRegistryMap = componentRM;
					itemFound = true;
					_componentStack.add(componentRM);
					break;
				}
			}
		}

		return itemFound;
	}

	public boolean getdeeplinkComponentItem(String destinationUrl) {
		if (null == destinationUrl) {
			return false;
		}
		boolean itemFound = false;
		String urlToUse = destinationUrl.toLowerCase();

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			for (String urlPart : componentRM.getParturls()) {
				urlPart = urlPart.toLowerCase();
				urlToUse = urlToUse.toLowerCase();
				if ((urlToUse.contains(urlPart))||(urlPart.contains(urlToUse))) {
					LH_ComponentRegistry._lastAccessComponentRegistryMap = componentRM;
					itemFound = true;
					//_componentStack.add(componentRM);

					break;
				}
			}
		}

		return itemFound;
	}

	public boolean setLastUserInteractionItemOnBackClick(String title) {
		if (null == title) {
			return false;
		}
		boolean itemFound = false;
		String titleToUse = title.toLowerCase();

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			if (null != componentRM.getTitle()
					&& componentRM.getTitle().length() > 0) {
				String componentTitle = componentRM.getTitle().toLowerCase();
				if (titleToUse.contains(componentTitle)) {
					LH_ComponentRegistry._lastAccessComponentRegistryMap = componentRM;
					itemFound = true;
					//_componentStack.add(componentRM);
					break;
				}
			}
		}

		return itemFound;
	}

	public String getLastUserInteractionItemTitle() {
		String titleToReturn = "";

		if (null != LH_ComponentRegistry._lastAccessComponentRegistryMap) {
			titleToReturn = LH_ComponentRegistry._lastAccessComponentRegistryMap.getTitle();
//		}else{
//			
//			titleToReturn = LHApplication.Menu.getLhmenuitemList().get(0)
//					.getTitle();
//		}
		}
		return titleToReturn;
	}

	public LH_ComponentRegistryMap getComponent(String destinationUrl) {

		LH_ComponentRegistryMap componentRegistryMap = null;
		if (null == destinationUrl) {
			return componentRegistryMap;
		}

		boolean itemFound = false;
		String urlToUse = destinationUrl.toLowerCase();

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			// String urlPart = componentRM.getKeyPartUrl().toLowerCase();
			for (String urlPart : componentRM.getParturls()) {
				if (urlToUse.contains(urlPart)) {
					componentRegistryMap = componentRM;
					itemFound = true;
					break;
				}
			}
			// urlPart = componentRM.getDestinationPartUrl().toLowerCase();
			// if (urlToUse.contains(urlPart)) {
			// componentRegistryMap = componentRM;
			// itemFound = true;
			// break;
			// }
		}

		if (itemFound) {
			if ((null != LH_ComponentRegistry._lastAccessComponentRegistryMap)
					&& LH_ComponentRegistry._lastAccessComponentRegistryMap.getTitle()
							.equalsIgnoreCase(componentRegistryMap.getTitle())) {
				componentRegistryMap = null;
			}
		}

		return componentRegistryMap;
	}

	public String getTitleForUrl(String destinationUrl) {
		String titleToReturn = "";

		if (null == destinationUrl) {
			return titleToReturn;
		}

		String urlToUse = destinationUrl.toLowerCase();

		for (LH_ComponentRegistryMap componentRM : LH_ComponentRegistry._componentRegistryMap) {
			// String urlPart = componentRM.getKeyPartUrl().toLowerCase();
			for (String urlPart : componentRM.getParturls()) {
				if (urlToUse.contains(urlPart)) {
					titleToReturn = componentRM.getTitle();
					break;
				}
			}
			// urlPart = componentRM.getDestinationPartUrl().toLowerCase();
			// if (urlToUse.contains(urlPart)) {
			// titleToReturn = componentRM.getTitle();
			// break;
			// }
		}

		return titleToReturn;

	}

	public LH_ComponentRegistryMap getComponentIfRedirectedToLogin(
			String destinationUrl) {
		LH_ComponentRegistryMap currentAccessComponentRegistryMap = null;
		if (null == destinationUrl) {
			return currentAccessComponentRegistryMap;
		}
		String urlToInterrogate = destinationUrl.toLowerCase();

		if (LHApplication.LoginPageRedirectCounter < 2) {
			// If not then check if the new url is a result of timeout redirect
			// to login page
			for (String urlItem : SpecialPages.PAGES_FOR_TIMEOUT_REFRESH) {
				if (urlToInterrogate.matches(".*" + urlItem + ".*")) {
					// If this is timeout redirect to login page then make a
					// fresh request to the original url and increment the
					// counter for timeout
					currentAccessComponentRegistryMap = LH_ComponentRegistry._lastAccessComponentRegistryMap;
					LHApplication.LoginPageRedirectCounter = LHApplication.LoginPageRedirectCounter + 1;
					break;
				}
			}

		}

		return currentAccessComponentRegistryMap;
	}

	public LH_ComponentRegistryMap getComponentIfRedirectedToLoginMoreThanTwoTimes(
			String destinationUrl) {
		LH_ComponentRegistryMap currentAccessComponentRegistryMap = null;
		if (null == destinationUrl) {
			return currentAccessComponentRegistryMap;
		}
		String urlToInterrogate = destinationUrl.toLowerCase();

		if (LHApplication.LoginPageRedirectCounter >= 2) {
			// If not then check if the new url is a result of timeout redirect
			// to login page
			for (String urlItem : SpecialPages.PAGES_FOR_BREAKING_OUT_TO_NATIVE_LOGIN) {
				if (urlToInterrogate.matches(".*" + urlItem + ".*")) {
					// If this is timeout redirect to login page then make a
					// fresh request to the original url and increment the
					// counter for timeout
					currentAccessComponentRegistryMap = new LH_ComponentRegistryMap();
					List<String> parturls = new ArrayList<>();
					parturls.add("");
					currentAccessComponentRegistryMap.setParturls(parturls);
					// currentAccessComponentRegistryMap.setKeyPartUrl("");
					// currentAccessComponentRegistryMap.setDestinationPartUrl("");
					currentAccessComponentRegistryMap.setDestinationFullUrl("");
					currentAccessComponentRegistryMap.setTitle("loginHome");
					currentAccessComponentRegistryMap
							.setType(FeatureType.NATIVE);

					LHApplication.LoginPageRedirectCounter = LHApplication.LoginPageRedirectCounter + 1;
					break;
				}
			}

		}

		return currentAccessComponentRegistryMap;
	}

	public LH_ComponentRegistryMap getComponentEmailRedirect(
			String destinationUrl) {
		LH_ComponentRegistryMap currentAccessComponentRegistryMap = null;
		if (null == destinationUrl) {
			return currentAccessComponentRegistryMap;
		}
		String urlToInterrogate = destinationUrl.toLowerCase();

		// for(String urlItem: SpecialPages.PAGES_FOR_EMAIL) {
		if (urlToInterrogate
				.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
			currentAccessComponentRegistryMap = new LH_ComponentRegistryMap();

			List<String> parturls = new ArrayList<>();
			parturls.add(destinationUrl);
			currentAccessComponentRegistryMap.setParturls(parturls);
			// currentAccessComponentRegistryMap
			// .setDestinationPartUrl(destinationUrl);
			currentAccessComponentRegistryMap
					.setDestinationFullUrl(destinationUrl);
			currentAccessComponentRegistryMap.setTitle("Email");
			currentAccessComponentRegistryMap.setType(FeatureType.NATIVE);
			// break;
		}
		// }

		return currentAccessComponentRegistryMap;
	}

	public Stack<LH_ComponentRegistryMap> getLastAccessComponentStack() {
		return _componentStack;
	}

	public void resetLastAccessComponentStack() {
		if (null != _componentStack) {
			_componentStack.removeAllElements();
		} else {
			_componentStack = new Stack<LH_ComponentRegistryMap>();
		}
	}

	public String getPreviousAccessedComponentTitle() {
		String prevTitle = "";
		boolean prevFound = false;

		if (null != _componentStack) {
			try {
				LH_ComponentRegistryMap prevComponentPeeked = null;

				try {
					LHApplication.ComponentRegistry
							.getLastAccessComponentStack().pop();
				} catch (Exception e) {
					prevTitle = LHApplication.Menu.getLhmenuitemList().get(0)
							.getTitle();
				
					LHApplication.emptystack=true;
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					prevComponentPeeked = LHApplication.ComponentRegistry
							.getLastAccessComponentStack().peek();
				} catch (Exception e) {
					prevTitle = LHApplication.Menu.getLhmenuitemList().get(0)
							.getTitle();
					LHApplication.emptystack=true;
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				if (null != prevComponentPeeked) {
					prevTitle = prevComponentPeeked.getTitle();
					prevFound = true;
				}
			} catch (EmptyStackException exp) {
				exp.printStackTrace();
				prevTitle = LHApplication.Menu.getLhmenuitemList().get(0)
						.getTitle();
				return prevTitle;
			}

			if (!prevFound && null != LHApplication.Menu
					&& LHApplication.Menu.getLhmenuitemList().size() > 0) {
				prevTitle = LHApplication.Menu.getLhmenuitemList().get(0)
						.getTitle();
			}
		}

		return prevTitle;
	}

	public LH_ComponentRegistryMap setComponentRegistryItem(
			LH_ComponentRegistryItems ComponentRegistry) {

		LH_ComponentRegistryMap componentRegistryMap = new LH_ComponentRegistryMap();
		componentRegistryMap
				.set_featureName(ComponentRegistry.getFeatureName());

		String urls = ComponentRegistry.getUrlParts();
		StringTokenizer st = new StringTokenizer(urls, "||");
		List<String> parturls = new ArrayList<>();
		while (st.hasMoreElements()) {
		
			String urlToken = st.nextElement().toString();
			parturls.add(urlToken);

			for (LH_MenuItems Menu : LHApplication.Menu.getLhmenuitemList()) {

				// LH_LogHelper.logInfo("Menu url loop -"
				// + Menu.getUrl().toString());

				if (Menu.getUrl().toString()
						.contains(urlToken)) {

//					int index = LHApplication.Menu.getLhmenuitemList().indexOf(
//							Menu);

					String Title = Menu.getTitle().toString();
					String Url = Menu.getUrl().toString();
					componentRegistryMap.setTitle(Title);
					
				}

			}

		}

		componentRegistryMap.setParturls(parturls);
		// componentRegistryMap.setDestinationPartUrl(ComponentRegistry.getUrlParts());
		componentRegistryMap.setDestinationFullUrl(ComponentRegistry
				.getUrlParts());
		componentRegistryMap.setType(ComponentRegistry.getFeatureType());
		componentRegistryMap.set_featureSource(ComponentRegistry
				.getFeatureSource());
		componentRegistryMap.set_componentRegistryId(ComponentRegistry
				.getComponentRegistryId());
		componentRegistryMap.set_isComponentRegistry(true);

		// this._componentRegistryMap.add(componentRegistryMap);

		return componentRegistryMap;

	}
}