package com.livehealthier.mobile.http;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;

import com.livehealthier.mobile.utils.LH_LogHelper;

public class LHHttpConnection {
	public static String _lhPostconnection(String url, List<NameValuePair> pairs)
			throws IOException {

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		String responseText = null;

		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
			try {
				HttpResponse response = client.execute(post);

				responseText = EntityUtils.toString(response.getEntity());
				LH_LogHelper.logInfo(responseText);

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseText;

	}

	public static String getResponseFromGetRequest(String preurl, String token)
			throws IOException {

		URL url = null;
		InputStream in = null;
		String preresponseText = null;

		try {
			url = new URL(preurl);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpURLConnection urlConnection = null;
		try {
			urlConnection = (HttpURLConnection) url.openConnection();
		
			if (null != token &&token.length() > 1) {

				urlConnection.setRequestProperty("MobileUserAuthHandle", token);
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			in = new BufferedInputStream(urlConnection.getInputStream());
			preresponseText = readStream(in);
			// preresponseText = "{"+preresponseText+"}";

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			urlConnection.disconnect();
		}

		return preresponseText;

	}

	private static String readStream(InputStream in) throws IOException {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();

		try {

			BufferedReader r = new BufferedReader(new InputStreamReader(in),
					1024 * 8);
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				sb.append(line);
			}
		} catch (Exception e) {
		}

		return sb.toString();
	}

	private Context _context;

	public LHHttpConnection(Context context) {
		this._context = context;
	}
}
