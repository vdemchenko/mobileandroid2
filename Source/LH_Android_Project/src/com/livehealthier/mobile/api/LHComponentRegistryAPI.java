package com.livehealthier.mobile.api;

import java.util.ArrayList;

import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.activity.LHSplashScreen;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistry;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistryMap;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_ComponentRegistryItems;
import com.livehealthier.mobile.model.LH_ComponentRegistryModel;
import com.livehealthier.mobile.model.LH_MenuItems;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class LHComponentRegistryAPI extends AsyncTask<String, String, String> {
String url;
public static LH_ComponentRegistry ComponentRegistry;
//String token;
LH_ComponentRegistryModel componentRegistryModel;
private ArrayList<LH_ComponentRegistryMap> _componentRegistryMap;

	public LHComponentRegistryAPI(String url){
		this.url=url;
		//this.token=token;
		_componentRegistryMap = new ArrayList<LH_ComponentRegistryMap>();
		
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		String _response = null;
		try {
			_response = LHHttpConnection.getResponseFromGetRequest(url,"");
			LH_LogHelper.logInfo(_response);
			try {
				componentRegistryModel = LH_Utils.componentRegisterparser(_response);
				
				for (LH_ComponentRegistryItems ComponentRegistryItems : componentRegistryModel
						.getComponentRegistryItems()) {
					
					LH_ComponentRegistryMap ComponentRegistryMap;
				
					ComponentRegistryMap =	LHApplication.ComponentRegistry.setComponentRegistryItem(ComponentRegistryItems);
					_componentRegistryMap.add(ComponentRegistryMap);
				}
			
				LHSplashScreen.saveComponentRegistry(_componentRegistryMap);
				LHApplication.ComponentRegistry.initializeRegistry();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return null;
	}

}
