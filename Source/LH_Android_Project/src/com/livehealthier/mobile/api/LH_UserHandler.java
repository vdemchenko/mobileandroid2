package com.livehealthier.mobile.api;

import android.app.Activity;

import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_Login;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LH_UserHandler {

	public LH_Login getUser(Activity context) {
		LH_Login user = null;
		boolean userAccessedSuccessfully = false;

		try {

			String token = LH_Utils.getWebToken(context);

			LH_LogHelper.logInfo("User Token - " + token);

			String getResponse = LHHttpConnection.getResponseFromGetRequest(
					LHApplication.CurrentEnvironment.getApiBaseUrl()
							+ LH_ApiUrls.USERAPI, token);

			LH_LogHelper.logInfo("get user response - " + getResponse);

			try {
				user = LH_Utils.loginParser(getResponse, "Login");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (null != user && user.getStatus().equalsIgnoreCase("success")
					&& user.getUser().size() > 0) {
				LHApplication.LoginPageRedirectCounter = 0;

				// note we do not receive token and session id when using
				// user/me end-point of the api
				// these values are received only during an authentication call
				// Consumer should be careful not to use session id as this will
				// fail in autologin flow
				user.getUser().get(0).setToken(token);
				userAccessedSuccessfully = true;

				try {
					LH_Utils.deleteUsertable(context);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				try {
					LH_Utils.saveLoginInformation(context, user);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (!userAccessedSuccessfully) {
				LH_Utils.savelogintoken(context, "livehealthier");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LH_Utils.savelogintoken(context, "livehealthier");
		}

		return user;
	}

}
