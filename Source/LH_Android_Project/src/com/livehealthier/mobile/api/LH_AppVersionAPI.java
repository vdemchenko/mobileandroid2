package com.livehealthier.mobile.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_AppVersion;
import com.livehealthier.mobile.model.LH_AppVersionModel;
import com.livehealthier.mobile.model.LH_ComponentRegistryModel;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LH_AppVersionAPI extends AsyncTask<String, Integer, Integer> {
	LH_AppVersion AppVersionModel;
	Context _context;
	String versionName;
	String IsUpgradeOptional;
	String MobileAppVersionId;
	String NumberOfDaysGapBetweenMessageDisplay;
	String UpgradeMessage;
	String VersionText;
	String loginText;
	private static final String SHARED_PREFS_APPVERSION = "APPVersionDateInformation";
	String AppVersion = "";
	
	
	public LH_AppVersionAPI(Context context) {
		_context = context;
	}

	public void getAppVersionInfo(Context context) {
		try {

			//String token = LH_Utils.getWebToken(context);

			

			try {
				AppVersion = _context
						.getApplicationContext()
						.getPackageManager()
						.getPackageInfo(
								_context.getApplicationContext()
										.getPackageName(), 0).versionName;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (null == AppVersion) {
				AppVersion = _context.getApplicationContext().getString(
						R.string.Versionname);

				// cookieManager.setCookie(urlRequested, "AppVersion=" +
				// AppVersion);
			}

			// LH_LogHelper.logInfo("User Token On Logout " + token);

			String response = LHHttpConnection
					.getResponseFromGetRequest(
							LHApplication.CurrentEnvironment.getApiBaseUrl()
									+ LH_ApiUrls.MOBILEAPPVERSIONID
									+ AppVersion, "");

			LH_LogHelper.logInfo("AppVersion response - " + response);

			AppVersionModel = LH_Utils.appVersionparser(response);

			List<LH_AppVersionModel> LH_AppVersionModel = AppVersionModel
					.getAppVersion();
			versionName = LH_AppVersionModel.get(0).getVersionName();
			IsUpgradeOptional = LH_AppVersionModel.get(0)
					.getIsUpgradeOptional();
			MobileAppVersionId = LH_AppVersionModel.get(0)
					.getMobileAppVersionId();
			NumberOfDaysGapBetweenMessageDisplay = LH_AppVersionModel.get(0)
					.getNumberOfDaysGapBetweenMessageDisplay();
			UpgradeMessage = LH_AppVersionModel.get(0).getUpgradeMessage();
			VersionText = LH_AppVersionModel.get(0).getVersionText();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Integer doInBackground(String... params) {
		
		 loginText = params[0];
		if(loginText.equalsIgnoreCase("Login")){
			getAppVersionInfo(_context);
		}else{
			if(LHApplication.checkDateDifferent()){
				
				getAppVersionInfo(_context);
			}
			
		}
		
		
		
		return 1;
	}


	@Override
     protected void onPostExecute(Integer result) {
		 Float applicationVersion = 0.0f; 
		 Float serverVersion = 0.0f;
		  try {
			applicationVersion = Float.parseFloat(AppVersion);
			 
			if(versionName.length()>0){
			serverVersion = Float.parseFloat(versionName);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
			displayAlertOptional();
			updateAppVersionDate();
		 if(serverVersion>applicationVersion){
			
			
			if(IsUpgradeOptional.equalsIgnoreCase("true")){
			
				if(getNumberOfDaysGapBetweenMessageDisplay()){
					
					displayAlertOptional();
					
					updateAppVersionDate();
				}
			
				
				
			}else{
				
				 displayAlert();
			} 
			
			 
			 
		 }else{
			 
			 //Do nothing
		 }
		 
		 
	 }

	private Boolean getNumberOfDaysGapBetweenMessageDisplay() {
		Date currentTime = new Date();
		SharedPreferences prefs = _context.getSharedPreferences(
				SHARED_PREFS_APPVERSION, Context.MODE_PRIVATE);
		//Editor editor = prefs.edit();
		Date PreviousTime = null;
		String PreviousDate = prefs.getString("AppVersionDate", "");
//		editor.putString("Date", currentTime.toString());
//		editor.commit();
		if (PreviousDate == "") {

			return false;

		}

		SimpleDateFormat format = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss zzz yyyy");

		try {
			PreviousTime = format.parse(PreviousDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long diff = currentTime.getTime() - PreviousTime.getTime();

		long diffDays = diff / (24 * 60 * 60 * 1000) ;

		int NumberOfDays = Integer.parseInt(NumberOfDaysGapBetweenMessageDisplay);
		
		if (diffDays > NumberOfDays) {
			return true;
		} else {
			return false;
		}

	}
	

	private Boolean updateAppVersionDate() {
		Date currentTime = new Date();
		SharedPreferences prefs = _context.getSharedPreferences(
				SHARED_PREFS_APPVERSION, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		//Date PreviousTime = null;
		//String PreviousDate = prefs.getString("Date", "");
		editor.putString("AppVersionDate", currentTime.toString());
		 editor.commit();
		 return true;
		
	}

	@SuppressWarnings("deprecation")
	private void displayAlertOptional() {
		 final AlertDialog alertDialog = new AlertDialog.Builder(
				_context).create();
		alertDialog.setTitle(_context.getResources().getString(
				R.string.app_name));
		alertDialog.setMessage(UpgradeMessage);
		alertDialog.setCancelable(false);
		alertDialog.setButton(
				_context.getResources().getString(
						R.string.Update),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {

						final String appPackageName = _context.getPackageName(); // getPackageName() from Context or Activity object
						try {
							_context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (final android.content.ActivityNotFoundException anfe) {
							_context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
						}

					}
				});

		alertDialog.setButton2(
				_context.getResources().getString(R.string.Skip),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						alertDialog.cancel();

					}
				});

		alertDialog.show();
		
	}

	@SuppressWarnings("deprecation")
	private void displayAlert() {
		final AlertDialog alertDialog = new AlertDialog.Builder(
				_context).create();
		alertDialog.setTitle(_context.getResources().getString(
				R.string.app_name));
		alertDialog.setMessage(UpgradeMessage);
		alertDialog.setCancelable(false);
		alertDialog.setButton(
				_context.getResources().getString(
						R.string.Update),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						final String appPackageName = _context.getPackageName(); // getPackageName() from Context or Activity object
						try {
							_context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
							_context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
						}

					}
				});

	

		alertDialog.show();
		
	}

}
