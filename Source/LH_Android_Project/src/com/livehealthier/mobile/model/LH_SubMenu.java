package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_Utils;

public class LH_SubMenu {
	// @JsonProperty("UniqueKey")
	private String UniqueKey;
	@JsonProperty("Title")
	private String Title;
	@JsonProperty("HasSubMenu")
	private String HasSubMenu;
	// @JsonProperty("Type")
	private String Type;
	@JsonProperty("url")
	private String url;
	@JsonProperty("SubMenu")
	private List<LH_SubMenu> SubMenu;

	public String getHasSubMenu() {
		return HasSubMenu;
	}

	public List<LH_SubMenu> getMenuItems() {
		return SubMenu;
	}

	public String getTitle() {
		return Title;
	}

	public String getType() {
		// return Type;
		// if ((!getUrl().equals(null)) && getUrl().length() > 0) {
		// return "";
		// }
		// else {
		if (getHasSubMenu().equalsIgnoreCase("true")) {
			return "";
		} else if (getUrl()
				.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
			return "native";
		} else {
			return "webview";
		}
		// }
	}

	public String getUniqueKey() {
		// return UniqueKey;
		return LH_Utils.getUniqueKeyForMenuItem(Title);
	}

	public String getUrl() {
		return url;
	}

	public void setHasSubMenu(String hasSubMenu) {
		HasSubMenu = hasSubMenu;
	}

	public void setMenuItems(List<LH_SubMenu> SubMenu) {
		this.SubMenu = SubMenu;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public void setType(String type) {
		Type = type;
	}

	public void setUniqueKey(String uniqueKey) {
		UniqueKey = uniqueKey;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
