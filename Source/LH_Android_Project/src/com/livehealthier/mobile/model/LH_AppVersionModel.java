package com.livehealthier.mobile.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_AppVersionModel {

	@JsonProperty("mobileAppVersionId")
	private String mobileAppVersionId;
	@JsonProperty("versionName")
	private String versionName;
	@JsonProperty("versionText")
	private String versionText;
	@JsonProperty("isUpgradeOptional")
	private String isUpgradeOptional;
	@JsonProperty("numberOfDaysGapBetweenMessageDisplay")
	private String numberOfDaysGapBetweenMessageDisplay;
	@JsonProperty("upgradeMessage")
	private String upgradeMessage;
	@JsonProperty("sourceAppVersionId")
	private String sourceAppVersionId;
	public String getMobileAppVersionId() {
		return mobileAppVersionId;
	}
	public void setMobileAppVersionId(String mobileAppVersionId) {
		this.mobileAppVersionId = mobileAppVersionId;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getVersionText() {
		return versionText;
	}
	public void setVersionText(String versionText) {
		this.versionText = versionText;
	}
	public String getIsUpgradeOptional() {
		return isUpgradeOptional;
	}
	public void setIsUpgradeOptional(String isUpgradeOptional) {
		this.isUpgradeOptional = isUpgradeOptional;
	}
	public String getNumberOfDaysGapBetweenMessageDisplay() {
		return numberOfDaysGapBetweenMessageDisplay;
	}
	public void setNumberOfDaysGapBetweenMessageDisplay(
			String numberOfDaysGapBetweenMessageDisplay) {
		this.numberOfDaysGapBetweenMessageDisplay = numberOfDaysGapBetweenMessageDisplay;
	}
	public String getUpgradeMessage() {
		return upgradeMessage;
	}
	public void setUpgradeMessage(String upgradeMessage) {
		this.upgradeMessage = upgradeMessage;
	}
	public String getSourceAppVersionId() {
		return sourceAppVersionId;
	}
	public void setSourceAppVersionId(String sourceAppVersionId) {
		this.sourceAppVersionId = sourceAppVersionId;
	}
	
	
	
}
