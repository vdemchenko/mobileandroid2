package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LH_Menu {
	@JsonProperty("Status")
	private String Status;
	@JsonProperty("MenuItems")
	private List<LH_MenuItems> MenuItems;
	@JsonProperty("exception")
	private String exception;

	public LH_Menu() {
		super();
	}

	public String getException() {
		return exception;
	}

	public List<LH_MenuItems> getLhmenuitemList() {
		return MenuItems;
	}

	public String getStatus() {
		return Status;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public void setLhmenuitemList(List<LH_MenuItems> MenuItems) {
		this.MenuItems = MenuItems;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	@Override
	public String toString() {
		return "LH_Menu [Status=" + Status + ", MenuItems=" + MenuItems
				+ ", exception=" + exception + "]";
	}

}
