package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_AppVersion {
	
	@JsonProperty("AppVersion")
	private List<LH_AppVersionModel> AppVersion;
	
	@JsonProperty("Status")
	private String Status;
	@JsonProperty("exception")
	private String exception;
	
	public List<LH_AppVersionModel> getAppVersion() {
		return AppVersion;
	}
	public void setAppVersion(List<LH_AppVersionModel> appVersion) {
		AppVersion = appVersion;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	
}
