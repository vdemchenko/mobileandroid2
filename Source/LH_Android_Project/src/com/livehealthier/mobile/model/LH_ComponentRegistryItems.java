package com.livehealthier.mobile.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_ComponentRegistryItems {

	@JsonProperty("componentRegistryId")
	private String componentRegistryId;
	
	@JsonProperty("urlParts")
	private String urlParts;
	
	@JsonProperty("featureName")
	private String featureName;
	
	@JsonProperty("featureType")
	private String featureType;
	
	@JsonProperty("featureSource")
	private String featureSource;

	public String getComponentRegistryId() {
		return componentRegistryId;
	}

	public void setComponentRegistryId(String componentRegistryId) {
		this.componentRegistryId = componentRegistryId;
	}

	public String getUrlParts() {
		return urlParts;
	}

	public void setUrlParts(String urlParts) {
		this.urlParts = urlParts;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	public String getFeatureSource() {
		return featureSource;
	}

	public void setFeatureSource(String featureSource) {
		this.featureSource = featureSource;
	}
	
	

	
	
	
}
