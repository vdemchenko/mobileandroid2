package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_Login {
	@JsonProperty("Status")
	private String Status;
	@JsonProperty("exception")
	private String exception;
	@JsonProperty("user")
	private List<LH_User> user;

	public String getException() {
		return exception;
	}

	public String getStatus() {
		return Status;
	}

	public List<LH_User> getUser() {
		return user;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	public void setUser(List<LH_User> user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "LH_Login [Status=" + Status + ", Users=" + ", exception="
				+ exception + ", user=" + user + "]";
	}
}
