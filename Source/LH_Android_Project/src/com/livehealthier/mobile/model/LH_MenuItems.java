package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_Utils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LH_MenuItems {
	// @JsonProperty("UniqueKey")
	private String UniqueKey;
	@JsonProperty("Title")
	private String Title;
	@JsonProperty("HasSubMenu")
	private String HasSubMenu;
	// @JsonProperty("Type")
	private String Type;
	@JsonProperty("url")
	private String url;
	@JsonProperty("SubMenu")
	private List<LH_SubMenu> SubMenu;

	public LH_MenuItems() {
		super();
	}

	public String getHasSubMenu() {
		return HasSubMenu;
	}

	public List<LH_SubMenu> getSubMenu() {
		return SubMenu;
	}

	public String getTitle() {
		return Title;
	}

	public String getType() {
		// if ((!getUrl().equals(null)) && getUrl().length() > 0) {
		// return "";
		// }
		// else {
		if (getHasSubMenu().equalsIgnoreCase("true")) {
			return "";
		} else if (getUrl()
				.startsWith(LH_Constants.SpecialPages.PAGE_FOR_EMAIL)) {
			return "native";
		} else {
			return "webview";
		}
		// }
	}

	public String getUniqueKey() {
		// return UniqueKey;
		return LH_Utils.getUniqueKeyForMenuItem(Title);
	}

	public String getUrl() {
		return url;
	}

	public void setHasSubMenu(String hasSubMenu) {
		this.HasSubMenu = hasSubMenu;
	}

	public void setSubMenu(List<LH_SubMenu> subMenu) {
		SubMenu = subMenu;
	}

	public void setTitle(String title) {
		this.Title = title;
	}

	public void setType(String type) {
		this.Type = type;
	}

	public void setUniqueKey(String uniqueKey) {
		this.UniqueKey = uniqueKey;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "LH_MenuItem [UniqueKey=" + UniqueKey + ", Title=" + Title
				+ ", HasSubMenu=" + HasSubMenu + ", Type=" + Type + ", url="
				+ url + ", SubMenu=" + SubMenu + "]";
	}
}
