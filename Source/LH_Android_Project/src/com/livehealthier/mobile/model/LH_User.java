package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_User {
	@JsonProperty("Id")
	private String Id;
	@JsonProperty("FirstName")
	private String FirstName;
	@JsonProperty("LastName")
	private String LastName;
	@JsonProperty("Email")
	private String Email;
	@JsonProperty("preferredEmail")
	private String preferredEmail;
	@JsonProperty("ExternalID")
	private String ExternalID;
	@JsonProperty("Token")
	private String Token;
	@JsonProperty("Status")
	private String Status;
	@JsonProperty("SessionId")
	private String SessionId;
	@JsonProperty("OfficeID")
	private String OfficeId;
	@JsonProperty("Office")
	private List<LH_Office> Office;

	public String getEmail() {
		return Email;
	}

	public String getExternalID() {
		return ExternalID;
	}

	public String getFirstName() {
		return FirstName;
	}

	public String getId() {
		return Id;
	}

	public String getLastName() {
		return LastName;
	}

	public List<LH_Office> getOffice() {
		return Office;
	}

	public String getOfficeId() {
		return OfficeId;
	}

	public String getPreferredEmail() {
		return preferredEmail;
	}

	public String getSessionId() {
		return SessionId;
	}

	public String getStatus() {
		return Status;
	}

	public String getToken() {
		return Token;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public void setExternalID(String externalID) {
		ExternalID = externalID;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public void setOfficeId(String officeId) {
		OfficeId = officeId;
	}

	public void setOffice(List<LH_Office> office) {
		this.Office = office;
	}

	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}

	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public void setToken(String token) {
		Token = token;
	}

	@Override
	public String toString() {
		return "LH_User [id=" + Id + ", FirstName=" + FirstName + ", LastName="
				+ LastName + ", Email=" + Email + ", preferredEmail="
				+ preferredEmail + ", ExternalID=" + ExternalID + ", Token="
				+ Token + ", Status=" + Status + ", SessionId=" + SessionId
				+ ", office=" + Office + "]";
	}

}
